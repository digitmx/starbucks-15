'use strict';
module.exports = function(grunt) {

    // load all grunt tasks
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    grunt.initConfig({

		watch: {
		    js: {
		        files: ['assets/js/functions.js'],
		        tasks: ['concat:js','uglify']
    		},
    		css: {
	    		files: ['assets/css/style.css'],
		        tasks: ['concat:css','cssmin']
    		}
		},

        copy: {
		  	main: {
		    	files: [
					// includes files within path and its sub-directories
					//{expand: true, src: ['favicon/*'], dest: 'img/'},
				],
		  	},
		},

        concat: {
            css: {
                src: [
                    'assets/css/material-icon.css','assets/css/materialize.min.css','assets/css/normalize.css','assets/css/style.css'
                ],
                dest: 'combined/combined.css'
            },
            js: {
                src: [
                    'assets/js/jquery-3.2.1.min.js','assets/js/materialize.min.js','assets/js/functions.js'
                ],
                dest: 'combined/combined.js'
            }
        },

        cssmin: {
            css: {
                src: 'combined/combined.css',
                dest: 'assets/css/app.css'
            }
        },

        uglify: {
            js: {
                files: {
                    'assets/js/app.js': ['combined/combined.js']
                }
            }
        },

        // image optimization
        imagemin: {
            dist: {
                options: {
                    optimizationLevel: 7,
                    progressive: true
                },
                files: [{
                    expand: true,
                    cwd: 'images/',
                    src: '**/*',
                    dest: 'assets/img/'
                }]
            }
        },

        clean: ["img/"]

    });

    // register task
    grunt.registerTask('default', ['clean', 'imagemin', 'concat', 'cssmin', 'uglify', 'copy']);

};