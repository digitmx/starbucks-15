<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Terminos extends CI_Controller {

	public function index()
	{
		//Carga de Vistas
		$this->load->view('includes/header');
		$this->load->view('terminos/index');
		$this->load->view('includes/footer');
	}
		
}
