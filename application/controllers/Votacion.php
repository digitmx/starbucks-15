<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Votacion extends CI_Controller {

	public function index()
	{
		//Create JSON Request
		$array = array(
			'msg' => 'semifinal'
		);
		$json_array = json_encode($array);

		//Request Call
		$response = $this->functions->call($json_array);
		$response_row = json_decode($response, true);
		
		$ip = $this->functions->get_client_ip_server();
		$ip_array = explode(',', $ip);
		$response_row['ip'] = $ip_array[0];
		
		//Carga de Vistas
		$this->load->view('includes/header');
		$this->load->view('votacion/index', $response_row);
		$this->load->view('includes/footer');
	}
	
	public function gracias()
	{
		//Carga de Vistas
		$this->load->view('includes/header');
		$this->load->view('votacion/gracias');
		$this->load->view('includes/footer');
	}
	
	public function error()
	{
		//Carga de Vistas
		$this->load->view('includes/header');
		$this->load->view('votacion/error');
		$this->load->view('includes/footer');
	}
	
	public function termino()
	{
		//Carga de Vistas
		$this->load->view('includes/header');
		$this->load->view('votacion/termino');
		$this->load->view('includes/footer');
	}
		
}
