<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		//Redirect Starbucks.com.mx
		redirect('http://www.starbucks.com.mx', 'refresh');
	}
}
