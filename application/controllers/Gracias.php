<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gracias extends CI_Controller {

	public function index()
	{
		//Carga de Vistas
		$this->load->view('includes/header');
		$this->load->view('gracias/index');
		$this->load->view('includes/footer');
	}
	
	public function verificacion()
	{
		//Carga de Vistas
		$this->load->view('includes/header');
		$this->load->view('gracias/verificacion');
		$this->load->view('includes/footer');
	}
		
}
