<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ganadores extends CI_Controller {

	public function index()
	{
		//Carga de Vistas
		$this->load->view('includes/header');
		$this->load->view('ganadores/index');
		$this->load->view('includes/footer');
	}
	
	public function video()
	{
		//Carga de Vistas
		$this->load->view('includes/header');
		$this->load->view('ganadores/video');
		$this->load->view('includes/footer');
	}
	
}