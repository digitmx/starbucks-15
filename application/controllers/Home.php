<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		//Leer Fecha
		$fecha = date('Y-m-d H:i:s');
		
		if ($fecha < '2017-10-18 12:00:00')
		{
			//Carga de Vistas
			$this->load->view('includes/header');
			$this->load->view('home/waiting');
			$this->load->view('includes/footer');
		}
		else
		{
			//Carga de Vistas
			$this->load->view('includes/header');
			$this->load->view('ganadores/index');
			$this->load->view('includes/footer');
		}
	}
	
	public function review()
	{
		//Leer Fecha
		$fecha = date('Y-m-d H:i:s');
		
		if ($fecha < '2017-10-02 00:00:00')
		{
			//Carga de Vistas
			$this->load->view('includes/header');
			$this->load->view('home/index');
			$this->load->view('includes/footer');
		}
		else
		{
			//Carga de Vistas
			$this->load->view('includes/header');
			$this->load->view('home/review');
			$this->load->view('includes/footer');
		}
	}
		
}
