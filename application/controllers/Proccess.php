<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// How to send the response header with PHP
if (isset($_SERVER['HTTP_ORIGIN'])) {
		header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

		exit(0);
}

ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 10000);

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Common\Aws;
use Aws\Common\Enum\Size;
use Aws\S3\Enum\CannedAcl;
use Aws\Common\Enum\Region;
use Aws\Common\Exception\MultipartUploadException;
use Aws\S3\Model\MultipartUpload\UploadBuilder;
use Aws\Common\Model\MultipartUpload\AbstractUploadBuilder;

class Proccess extends CI_Controller {

	public $bucket = 'sb15'; //nombre del bucket

    private $_key = 'AKIAJJXOTGPPE4GLY5LA'; // tu key

    private $_secret = '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'; // tu secret

    public $s3 = null;

	public function __construct()
	{
		parent::__construct();
		
		$this->s3 = S3Client::factory(array(
			'version' => '2006-03-01',
			'region' => 'us-west-2',
            'credentials' => array(
                'key'    => $this->_key,
                'secret' => $this->_secret,
            )
        ));
	}
	
	public function index()
	{
		//Redirect Starbucks.com.mx
		redirect(base_url(), 'refresh');
	}
	
	public function create_user()
	{
		//Read Values
		$name = (isset($_POST['inputName'])) ? (string)trim($_POST['inputName']) : '';
		$lastname = (isset($_POST['inputLastname'])) ? (string)trim($_POST['inputLastname']) : '';
		$genre = (isset($_POST['inputGenre'])) ? (string)trim($_POST['inputGenre']) : '';
		$email = (isset($_POST['inputEmail'])) ? (string)trim($_POST['inputEmail']) : '';
		$state = (isset($_POST['inputState'])) ? (string)trim($_POST['inputState']) : '';
		$phone = (isset($_POST['inputPhone'])) ? (string)trim($_POST['inputPhone']) : '';
		$product = (isset($_POST['inputProduct'])) ? (string)trim($_POST['inputProduct']) : '';
		$history = (isset($_POST['inputHistory'])) ? (string)trim($_POST['inputHistory']) : '';
		$role = (isset($_POST['inputRole'])) ? (string)trim($_POST['inputRole']) : '';
		$life = (isset($_POST['inputLife'])) ? (string)trim($_POST['inputLife']) : '';
		
		//Create JSON Request
		$array = array(
			'msg' => 'createUser',
			'fields' => array(
				'name' => $name,
				'lastname' => $lastname,
				'genre' => $genre,
				'email' => $email,
				'state' => $state,
				'phone' => $phone,
				'product' => $product,
				'history' => $history,
				'role' => $role,
				'life' => $life
			)
		);
		$json_array = json_encode($array);

		//Request Call
		$response = $this->functions->call($json_array);
		$response_row = json_decode($response, true);
		
		//Check Request Call Status
		if ((int)$response_row['status'] == 1)
		{
			//Session Data
			$this->session->set_userdata("user", $response_row);
			
			//Redirect Media
			redirect( base_url() . 'gracias' );
		}
		else
		{
			//Session Error
			$this->session->set_userdata("errorFormHistory", $response_row);
			
			//Redirect Dashboard
			redirect( base_url() );
		}
	}
	
	public function add_media()
	{
		//Leemos el Tipo de Archivo
		$iduser = (isset($_POST['iduser'])) ? (string)trim($_POST['iduser']) : '';
		$mysbrewards = (isset($_POST['inputSB'])) ? (string)trim($_POST['inputSB']) : '';
		$contador_images = 0;
		$photo_one = '';
		$photo_two = '';
		$photo_three = '';
		$video = '';
		
		//Revisamos si existe un archivo
		if(isset($_FILES['images']['tmp_name'][0]))
		{			
			if ($_FILES['images']['tmp_name'][0] != '')
			{
				//Creamos la ruta del archivo
				$filename = explode('.', $_FILES['images']['name'][0]);
				$id_array = count($filename) - 1;
				$key = time() . '_'.$iduser.'.' . strtolower($filename[$id_array]);
				
				$key = 'images/' . $key;
				$fullPath = $_FILES['images']['tmp_name'][0];
				
				//Subimos a S3
				$result = $this->s3->upload(
		            $this->bucket, //bucket
		            $key, //key, unique by each object
		            fopen($fullPath, 'rb'), //where is the file to upload?
		            'public-read' //permissions
		        );
				$new_url = $result['ObjectURL'];
				$new_url = str_replace('%2F', '/', $new_url);
				$new_url = str_replace('https://sb15.s3-us-west-2.amazonaws.com', 'https://d1i0lh2figu58y.cloudfront.net', $new_url);
				
				$photo_one = $new_url;
			}
		}
		
		//Revisamos si existe un archivo
		if(isset($_FILES['images']['tmp_name'][1]))
		{			
			//Creamos la ruta del archivo
			$filename = explode('.', $_FILES['images']['name'][1]);
			$id_array = count($filename) - 1;
			$key = time() . '_'.$iduser.'.' . strtolower($filename[$id_array]);
			
			$key = 'images/' . $key;
			$fullPath = $_FILES['images']['tmp_name'][1];
			
			//Subimos a S3
			$result = $this->s3->upload(
	            $this->bucket, //bucket
	            $key, //key, unique by each object
	            fopen($fullPath, 'rb'), //where is the file to upload?
	            'public-read' //permissions
	        );
			$new_url = $result['ObjectURL'];
			$new_url = str_replace('%2F', '/', $new_url);
			$new_url = str_replace('https://sb15.s3-us-west-2.amazonaws.com', 'https://d1i0lh2figu58y.cloudfront.net', $new_url);
			
			$photo_two = $new_url;
		}
		
		//Revisamos si existe un archivo
		if(isset($_FILES['images']['tmp_name'][2]))
		{			
			//Creamos la ruta del archivo
			$filename = explode('.', $_FILES['images']['name'][2]);
			$id_array = count($filename) - 1;
			$key = time() . '_'.$iduser.'.' . strtolower($filename[$id_array]);
			
			$key = 'images/' . $key;
			$fullPath = $_FILES['images']['tmp_name'][2];
			
			//Subimos a S3
			$result = $this->s3->upload(
	            $this->bucket, //bucket
	            $key, //key, unique by each object
	            fopen($fullPath, 'rb'), //where is the file to upload?
	            'public-read' //permissions
	        );
			$new_url = $result['ObjectURL'];
			$new_url = str_replace('%2F', '/', $new_url);
			$new_url = str_replace('https://sb15.s3-us-west-2.amazonaws.com', 'https://d1i0lh2figu58y.cloudfront.net', $new_url);
			
			$photo_three = $new_url;
		}
		
		//Revisamos si existe un archivo
		if(isset($_FILES['video']['tmp_name'][0]))
		{			
			//Creamos la ruta del archivo
			$filename = explode('.', $_FILES['video']['name']);
			$id_array = count($filename) - 1;
			$key = time() . '_'.$iduser.'.mp4';
			
			$key = 'videos/' . $key;
			$fullPath = $_FILES['video']['tmp_name'];
			
			//Subimos a S3
			$result = $this->s3->upload(
	            $this->bucket, //bucket
	            $key, //key, unique by each object
	            fopen($fullPath, 'rb'), //where is the file to upload?
	            'public-read' //permissions
	        );
			$new_url = $result['ObjectURL'];
			$new_url = str_replace('%2F', '/', $new_url);
			$new_url = str_replace('https://sb15.s3-us-west-2.amazonaws.com', 'https://d1i0lh2figu58y.cloudfront.net', $new_url);
			
			$video = $new_url;
		}
		
		//Create JSON Request
		$array = array(
			'msg' => 'addMedia',
			'fields' => array(
				'iduser' => $iduser,
				'photo_one' => $photo_one,
				'photo_two' => $photo_two,
				'photo_three' => $photo_three,
				'video' => $video,
				'mysbrewards' => $mysbrewards
			)
		);
		$json_array = json_encode($array);

		//Request Call
		$response = $this->functions->call($json_array);
		$response_row = json_decode($response, true);
		
		//Check Request Call Status
		if ((int)$response_row['status'] == 1)
		{
			//Session Data
			$this->session->sess_destroy();
			
			//Redirect Media
			redirect( base_url() . 'gracias' );
		}
		else
		{
			//Session Error
			$this->session->set_userdata("errorFormMedia", $response_row);
			
			//Redirect Dashboard
			redirect( base_url() . 'registro' );
		}
	}
	
	public function create_history()
	{
		//Read Values
		$name = (isset($_POST['inputName'])) ? (string)trim($_POST['inputName']) : '';
		$lastname = (isset($_POST['inputLastname'])) ? (string)trim($_POST['inputLastname']) : '';
		$genre = (isset($_POST['inputGenre'])) ? (string)trim($_POST['inputGenre']) : '';
		$email = (isset($_POST['inputEmail'])) ? (string)trim($_POST['inputEmail']) : '';
		$state = (isset($_POST['inputState'])) ? (string)trim($_POST['inputState']) : '';
		$phone = (isset($_POST['inputPhone'])) ? (string)trim($_POST['inputPhone']) : '';
		$product = (isset($_POST['inputProduct'])) ? (string)trim($_POST['inputProduct']) : '';
		$history = (isset($_POST['inputHistory'])) ? (string)trim($_POST['inputHistory']) : '';
		$role = (isset($_POST['inputRole'])) ? (string)trim($_POST['inputRole']) : '';
		$sb1 = (isset($_POST['inputSB1'])) ? (string)trim($_POST['inputSB1']) : '';
		$sb2 = (isset($_POST['inputSB2'])) ? (string)trim($_POST['inputSB2']) : '';
		$sb3 = (isset($_POST['inputSB3'])) ? (string)trim($_POST['inputSB3']) : '';
		$sb4 = (isset($_POST['inputSB4'])) ? (string)trim($_POST['inputSB4']) : '';
		$mysbrewards = $sb1 . $sb2 . $sb3 . $sb4;
		$contador_images = 0;
		$photos = array();
		$video = '';
		
		//Images
		if(isset($_FILES['images']))
		{
			//Check Images
			foreach ($_FILES['images']['name'] as $image)
			{
				if($_FILES['images']['error'][$contador_images] == 0)
				{
					//Creamos la ruta del archivo
					$filename = explode('.', $_FILES['images']['name'][$contador_images]);
					$id_array = count($filename) - 1;
					$key = time() . '_'.random_string('nozero', 8).'.' . strtolower($filename[$id_array]);
					
					$key = 'images/' . $key;
					$fullPath = $_FILES['images']['tmp_name'][$contador_images];
					
					//Subimos a S3
					$result = $this->s3->upload(
			            $this->bucket, //bucket
			            $key, //key, unique by each object
			            fopen($fullPath, 'rb'), //where is the file to upload?
			            'public-read' //permissions
			        );
					$new_url = $result['ObjectURL'];
					$new_url = str_replace('%2F', '/', $new_url);
					$new_url = str_replace('https://sb15.s3-us-west-2.amazonaws.com', 'https://d1i0lh2figu58y.cloudfront.net', $new_url);
					
					$photos[] = $new_url;
				}
				$contador_images++;
			}
		}
		
		//Video
		if(isset($_FILES['video']))
		{
			//Revisamos si existe un archivo
			if($_FILES['video']['error'] == 0)
			{			
				//Creamos la ruta del archivo
				$filename = explode('.', $_FILES['video']['name']);
				$id_array = count($filename) - 1;
				$key = time() . '_'.random_string('nozero', 8).'.mp4';
				
				$key = 'videos/' . $key;
				$fullPath = $_FILES['video']['tmp_name'];
				
				//Subimos a S3
				$result = $this->s3->upload(
		            $this->bucket, //bucket
		            $key, //key, unique by each object
		            fopen($fullPath, 'rb'), //where is the file to upload?
		            'public-read' //permissions
		        );
				$new_url = $result['ObjectURL'];
				$new_url = str_replace('%2F', '/', $new_url);
				$new_url = str_replace('https://sb15.s3-us-west-2.amazonaws.com', 'https://d1i0lh2figu58y.cloudfront.net', $new_url);
				
				$video = $new_url;
			}
		}
		
		$photos = json_encode($photos);
		
		//Create JSON Request
		$array = array(
			'msg' => 'create_history',
			'fields' => array(
				'name' => $name,
				'lastname' => $lastname,
				'genre' => $genre,
				'email' => $email,
				'state' => $state,
				'phone' => $phone,
				'product' => $product,
				'history' => $history,
				'role' => $role,
				'photos' => $photos,
				'video' => $video,
				'mysbrewards' => $mysbrewards
			)
		);
		$json_array = json_encode($array);

		//Request Call
		$response = $this->functions->call($json_array);
		$response_row = json_decode($response, true);
		
		//Check Request Call Status
		if ((int)$response_row['status'] == 1)
		{
			//Asignamos nueva session
			$this->session->set_userdata("gracias", true);
			
			//Redirect Media
			redirect( base_url() . 'gracias' );
		}
		else
		{
			//Session Error
			$this->session->set_userdata("errorFormHistory", $response_row);
			
			//Redirect Dashboard
			redirect( base_url() );
		}
	}
	
	public function vote()
	{
		//Leer Fecha
		$fecha = date('Y-m-d H:i:s');
		
		if ($fecha < '2017-10-16 00:00:00')
		{
			//Leemos los datos
			$iduser = (isset($_POST['iduser'])) ? (string)trim($_POST['iduser']) : '';
			$ip_form = (isset($_POST['ip'])) ? (string)trim($_POST['ip']) : '';
			$ip = $this->functions->get_client_ip_server();
			$ip_array = explode(',', $ip);
			$ip = $ip_array[0];
			
			//Verificamos la IP
			if ($ip_form == $ip)
			{
				//Checamos IP valida
				if ($ip != '::1' && $ip != 'UNKNOWN')
				{
					//Create JSON Request
					$array = array(
						'msg' => 'vote',
						'fields' => array(
							'iduser' => $iduser,
							'ip' => $ip
						)
					);
					$json_array = json_encode($array);
			
					//Request Call
					$response = $this->functions->call($json_array);
					$response_row = json_decode($response, true);
					
					//Check Request Call Status
					if ((int)$response_row['status'] == 1)
					{
						//Redirect Media
						redirect( base_url() . 'votacion/gracias' );
					}
					else
					{
						//Redirect Dashboard
						redirect( base_url() . 'votacion/error' );
					}
				}
				else
				{
					//Redirect Dashboard
					redirect( base_url() . 'votacion/error' );
				}
			}
			else
			{
				//Redirect Dashboard
				redirect( base_url() . 'votacion/error' );
			}
		}
		else
		{
			//Redirect Dashboard
			redirect( base_url() . 'votacion/termino' );
		}
	}
	
}
