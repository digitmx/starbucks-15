<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verify extends CI_Controller {

	public function index($iduser = 0)
	{
		//Verify $id
		if ($iduser)
		{
			//Create JSON Request
			$array = array(
				'msg' => 'verifyUser',
				'fields' => array(
					'iduser' => $iduser
				)
			);
			$json_array = json_encode($array);
	
			//Request Call
			$response = $this->functions->call($json_array);
			$response_row = json_decode($response, true);
			
			//Check Request Call Status
			if ((int)$response_row['status'] == 1)
			{
				//Asignamos nueva session
				$this->session->set_userdata("verificacion", true);
			
				//Redirect Media
				redirect( base_url() . 'gracias/verificacion' );
			}
			else
			{
				//Redirect Dashboard
				redirect( base_url() );
			}
		}
		else
		{
			//Redirect Home
			redirect( base_url() );
		}
	}
	
}