<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Response extends CI_Controller {

	public function index()
	{
		//Check Facebook Response
		$response = $this->facebook->response();
		$params = ($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : '';
		$ip = '0.0.0.0';
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		    $ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
		    $ip = $_SERVER['REMOTE_ADDR'];
		}
		
		//Check Response
		if (is_array($response))
		{
			//Registramos al Usuario
			$data = array(
				'name' => $response['first_name'],
				'lastname' => $response['last_name'],
				'email' => $response['email'],
				'fbid' => $response['id'],
				'access_token' => $response['token'],
				'ip' => $ip,
			    'status' => 1
			);

			//Save Admin in $_SESSION
			$this->session->set_userdata('facebook', $data);
			
			//Redirect to Home
			redirect( base_url() );
		}
		else
		{
			$data = array();
			
			//Save Admin in $_SESSION
			$this->session->set_userdata('facebook', $data);
			
			//Redirect to Home
			redirect(base_url());
		}
	}
}