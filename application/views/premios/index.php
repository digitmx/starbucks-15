
		<div class="fixed-action-btn">
			<a class="btn-large open-sans-regular font14" href="<?php echo base_url(); ?>#formulario">
				¡Registra y envía tu historia aquí!
    		</a>
  		</div>		
		<!--Banner Header Premios-->
		<div class="container-fluid header-premios">
			<div class="container-fluid">
				<div id="videoBlock" class="hide-on-med-and-down">
					<video preload="preload"  id="video" autoplay="autoplay" loop="loop" class="fillWidth">
						<source src="<?php echo base_url(); ?>assets/video/premios/Premios_bg_1280x720.mp4" type="video/mp4"></source>
					</video>
					<div id="videoMessage">
						<div class="row">
							<div class="space150-padding"></div>
							<div class="col s12 m12 l12 15-img">
								<a href="<?php echo base_url(); ?>">
									<img src="<?php echo base_url(); ?>assets/img/SB_Logo15_Hor.svg">
								</a>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 open-sans-light centered">
								<div class="row">
									<div class="col s12">
										<div class="space50-padding"></div>
										<span class="open-sans-regular font14 p-titulo">
											1ER. LUGAR
										</span>
										<div class="space30"></div>
										<p class="open-sans-regular font33">
											Viaje doble a Seattle para visitar la primera tienda Starbucks<br class="hide-on-med-and-down">
											y conocer el  <span class="open-sans-bold">Starbucks Reserve Roastery & Tasting Room</span>.
										</p>
										<p class="open-sans-regular font16">
											Disfruta de una experiencia multi-sensorial de la mano de expertos.<br class="hide-on-med-and-down">
											Descubre las notas de sabor de granos de café exóticos y cómo resaltarlas con diferentes métodos de preparación.<br class="hide-on-med-and-down"> 
											Una experiencia ideal para verdaderos fans del café de Starbucks.
										</p>
										<div class="space60"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="hero-mobile" class="hide-on-large-only">
					<div class="row">
						<div class="space150-padding"></div>
						<div class="col s12 m12 l12 15-img">
							<a href="<?php echo base_url(); ?>">
								<img src="<?php echo base_url(); ?>assets/img/SB_Logo15_Hor.svg">
							</a>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m12 l12 open-sans-light centered">
							<div class="row">
								<div class="col s12">
									<div class="space60"></div>
									<span class="open-sans-regular font14 p-titulo">
										1ER. LUGAR
									</span>
									<div class="space30"></div>
									<p class="open-sans-regular font33">
										Vive una experiencia multi-sensorial<br class="hide-on-med-and-down">
										en <span class="open-sans-bold">Roastery & Tasting Room</span> en Seattle. 
									</p>
									<p class="open-sans-regular font16">
										Conversa con expertos del café de Starbucks mientras disfrutas de granos exóticos<br class="hide-on-med-and-down">
										infusionados con distintos métodos de preparación.<br class="hide-on-med-and-down">
										Una experiencia sólo para verdaderos fans del café de Starbucks.
									</p>
									<div class="space60"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!--Premios-->
		<div class="container premios-secundarios">
			<div class="row">
				<div class="col s12 m12 l6 imagen-premios-e1 hide-on-med-and-down">
					<div class="slider centered">
					    <ul class="slides">
					    	<li>
					        	<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/2a.jpg">
							</li>
							<li>
					        	<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/2b.jpg">
							</li>
							<li>
					        	<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/2c.jpg">
							</li>
					    </ul>
					</div>
				</div>
				<div class="col s12 m12 l6 editar-texto1">
					<div class="space30"></div>
					<span class="open-sans-regular font14 p-titulo">
						2DO. LUGAR
					</span>
					<div class="space30"></div>
					<p class="open-sans-light font24">
						Descubre la <span class="open-sans-bold">Hacienda Alsacia</span> en Costa Rica,
						una finca experimental de Starbucks ubicada en las
						laderas del Volcán Poás en Alajuela.
					</p>
					<p class="open-sans-light font16">
						Experimenta todo el proceso de producción del café, desde
						la recolección, el tostado y molido, hasta su preparación.
					</p>
					<div class="space40"></div>
				</div>
				<div class="col s12 m12 l6 imagen-premios-e1 hide-on-large-only">
					<div class="slider centered">
					    <ul class="slides">
					    	<li>
					        	<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/2a.jpg">
							</li>
							<li>
					        	<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/2b.jpg">
							</li>
							<li>
					        	<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/2c.jpg">
							</li>
					    </ul>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col s12 m12 l6 editar-texto2">
					<div class="space40"></div>
					<span class="open-sans-regular font14 p-titulo">
						3ER. AL 15º LUGAR
					</span>
					<div class="space30"></div>
					<p class="open-sans-light font24">
						Descubre el Farmer Support Center de Starbucks en <span class="open-sans-bold">San Cristóbal de las Casas, Chiapas.</span><br>
					</p>
					<p class="open-sans-light font16">
						Viaje doble para conocer más sobre el mundo del café de la mano de nuestros expertos.
					</p>
				</div>
				<div class="col s12 m12 l6 imagen-premios-e2">
					<div class="slider centered">
					    <ul class="slides">
					    	<li>
					        	<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/3a.jpg">
							</li>
							<li>
					        	<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/3b.jpg">
							</li>
							<li>
					        	<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/3c.jpg">
							</li>
					    </ul>
					</div>
					<div class="space60"></div>
				</div>
			</div>
		</div>