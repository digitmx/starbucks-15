		
		<!--Banner Header Ganadores-->
		<div class="container-fluid header-votacion">
			<div class="container-fluid">
				<div class="row">
					<div class="space50-padding"></div>
					<div class="col s12 m2 offset-m1 l2 offset-l1 15-img">
						<a href="<?php echo base_url(); ?>">
							<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/SB_Logo15_Hor.svg">
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col s12 m12 l12 open-sans-light centered">
						<div class="space40"></div>
						<p class="titulo-finalistas font34">
							¡La votación terminó!
						</p>
						<p class="titulo-finalistas font20">
							Estas y todas las historias que recibimos, son el resultado de 15 años juntos.<br class="hide-on-small-only">
							Ustedes decidieron y estos son los lugares de las historias finalistas:
						</p>
					</div>
				</div>
			</div>
		</div>
		
		<!--Videos Ganadores-->
		<div class="container ganadores">
			<div class="row">
				<div class="col s12 m12 l6">
					<div class="texto-pos-gan">
						<h2 class="open-sans-bold font30 centered">
							1er <span class="open-sans-light">Lugar
						</h2>
						<p class="open-sans-light font16 centered ganadores-viaje">
							<br>Viaje Doble a Seattle
						</p>
					</div>
					<div class="ganadores">
						<div class="space20"></div>
						<div class="video-container">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/dXIea3FFjPM?autoplay=0&controls=1&fs=1&iv_load_policy=3&modestbranding=1&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="space20"></div>
						<div class="row open-sans-regular vot-box">
							<div class="col s8 m7 l8">
								<p class="font19" id="name">
									Gisela Rivera
								</p>
								<p class="font12 lavanda" id="state">
									Estado de México
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col s12 m12 l6">
					<div class="texto-pos-gan">
						<h2 class="open-sans-bold font30 centered">
							2do <span class="open-sans-light">Lugar
						</h2>
						<p class="open-sans-light font16 centered ganadores-viaje">
							<br>Viaje Doble a Costa Rica
						</p>
					</div>
					<div class="ganadores">
						<div class="space20"></div>
						<div class="video-container">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/WibD8dMKN9k?autoplay=0&controls=1&fs=1&iv_load_policy=3&modestbranding=1&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="space20"></div>
						<div class="row open-sans-regular vot-box">
							<div class="col s8 m7 l8">
								<p class="font19" id="name">
									Laura Belmar
								</p>
								<p class="font12 lavanda" id="state">
									Estado de México
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row tercer-lugar">
				<div class="col s2 m4 l4 linea-pos">
					<div class="linea-ganadopres"></div>
				</div>
				<div class="col s8 m4 l4 titulo-tercer">
					<p class="open-sans-light font30 centered">
						3er-15° Lugar
					</p>
					<p class="open-sans-light font16 centered">
						Viaje Doble a Chiapas
					</p>
				</div>
				<div class="col s2 m4 l4 linea-pos">
					<div class="linea-ganadopres"></div>
				</div>
			</div>
		</div>
		<div class="container-fluid tres-quince">
			<div class="row">
				<div class="col s12 m12 l10 offset-l1 centered">					
					<div class="col s12 m12 l4 centered">
						<div class="video-container">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/BES47K5yMyI?autoplay=0&controls=1&fs=1&iv_load_policy=3&modestbranding=1&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="row open-sans-regular vot-box">
							<div class="col s12">
								<p class="font19" id="name">
									Ángeles UB
								</p>
								<p class="font12 lavanda" id="state">
									Jalisco
								</p>
							</div>
						</div>
						<div class="space20"></div>
					</div>	
					<div class="col s12 m12 l4 centered">
						<div class="video-container">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/gr0S7t01BbU?autoplay=0&controls=1&fs=1&iv_load_policy=3&modestbranding=1&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="row open-sans-regular vot-box">
							<div class="col s12">
								<p class="font19" id="name">
									Moisés Sánchez
								</p>
								<p class="font12 lavanda" id="state">
									Ciudad de México
								</p>
							</div>
						</div>
						<div class="space20"></div>
					</div>
					<div class="col s12 m12 l4 centered">
						<div class="video-container">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/5hCSZbMss40?autoplay=0&controls=1&fs=1&iv_load_policy=3&modestbranding=1&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="row open-sans-regular vot-box">
							<div class="col s12">
								<p class="font19" id="name">
									Arturo Ayala
								</p>
								<p class="font12 lavanda" id="state">
									Puebla
								</p>
							</div>
						</div>
						<div class="space20"></div>
					</div>
					<div class="col s12 m12 l4 centered">
						<div class="video-container">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/k6Sw3FeYBdc?autoplay=0&controls=1&fs=1&iv_load_policy=3&modestbranding=1&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="row open-sans-regular vot-box">
							<div class="col s12">
								<p class="font19" id="name">
									José Padrón
								</p>
								<p class="font12 lavanda" id="state">
									Estado de México
								</p>
							</div>
						</div>
						<div class="space20"></div>
					</div>	
					<div class="col s12 m12 l4 centered">
						<div class="video-container">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/KB6OvbfOeBs?autoplay=0&controls=1&fs=1&iv_load_policy=3&modestbranding=1&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="row open-sans-regular vot-box">
							<div class="col s12">
								<p class="font19" id="name">
									Mariana Mora
								</p>
								<p class="font12 lavanda" id="state">
									Ciudad de México
								</p>
							</div>
						</div>
						<div class="space20"></div>
					</div>	
					<div class="col s12 m12 l4 centered">
						<div class="video-container">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/gT7i10VrbSQ?autoplay=0&controls=1&fs=1&iv_load_policy=3&modestbranding=1&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="row open-sans-regular vot-box">
							<div class="col s12">
								<p class="font19" id="name">
									Miriam Espino
								</p>
								<p class="font12 lavanda" id="state">
									Baja California Sur
								</p>
							</div>
						</div>
						<div class="space20"></div>
					</div>	
					<div class="col s12 m12 l4 centered">
						<div class="video-container">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/KSqrqrY-DNk?autoplay=0&controls=1&fs=1&iv_load_policy=3&modestbranding=1&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="row open-sans-regular vot-box">
							<div class="col s12">
								<p class="font19" id="name">
									Pamela Rodríguez
								</p>
								<p class="font12 lavanda" id="state">
									Nuevo León
								</p>
							</div>
						</div>
						<div class="space20"></div>
					</div>	
					<div class="col s12 m12 l4 centered">
						<div class="video-container">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/Nz8b-SCkCSY?autoplay=0&controls=1&fs=1&iv_load_policy=3&modestbranding=1&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="row open-sans-regular vot-box">
							<div class="col s12">
								<p class="font19" id="name">
									Germán Calderón
								</p>
								<p class="font12 lavanda" id="state">
									Puebla
								</p>
							</div>
						</div>
						<div class="space20"></div>
					</div>	
					<div class="col s12 m12 l4 centered">
						<div class="video-container">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/RO-mWt5UNgM?autoplay=0&controls=1&fs=1&iv_load_policy=3&modestbranding=1&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="row open-sans-regular vot-box">
							<div class="col s12">
								<p class="font19" id="name">
									Christian Canabal
								</p>
								<p class="font12 lavanda" id="state">
									Yucatán
								</p>
							</div>
						</div>
						<div class="space20"></div>
					</div>	
					<div class="col s12 m12 l4 centered">
						<div class="video-container">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/HOSAxT2ImyM?autoplay=0&controls=1&fs=1&iv_load_policy=3&modestbranding=1&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="row open-sans-regular vot-box">
							<div class="col s12">
								<p class="font19" id="name">
									Jesús Morales
								</p>
								<p class="font12 lavanda" id="state">
									Nuevo León
								</p>
							</div>
						</div>
						<div class="space20"></div>
					</div>	
					<div class="col s12 m12 l4 centered">
						<div class="video-container">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/EEuIr6-gRqI?autoplay=0&controls=1&fs=1&iv_load_policy=3&modestbranding=1&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="row open-sans-regular vot-box">
							<div class="col s12">
								<p class="font19" id="name">
									Eimy Hernández
								</p>
								<p class="font12 lavanda" id="state">
									Nuevo León
								</p>
							</div>
						</div>
						<div class="space20"></div>
					</div>	
					<div class="col s12 m12 l4 centered">
						<div class="video-container">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/h_DMTai6GNI?autoplay=0&controls=1&fs=1&iv_load_policy=3&modestbranding=1&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="row open-sans-regular vot-box">
							<div class="col s12">
								<p class="font19" id="name">
									Salomón Shayo
								</p>
								<p class="font12 lavanda" id="state">
									Ciudad de México
								</p>
							</div>
						</div>
						<div class="space20"></div>
					</div>	
					<div class="col s12 m12 l4 offset-l4 centered">
						<div class="video-container">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/oSVzhzyVSpo?autoplay=0&controls=1&fs=1&iv_load_policy=3&modestbranding=1&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="row open-sans-regular vot-box">
							<div class="col s12">
								<p class="font19" id="name">
									Juan Aguera
								</p>
								<p class="font12 lavanda" id="state">
									Quintana Roo
								</p>
							</div>
						</div>
						<div class="space20"></div>
					</div>	
				</div>
			</div>
			<div class="row">
			<div class="space60"></div>
			</div>
		</div>
		
