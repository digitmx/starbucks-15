		
		<!--Banner Header Ganadores-->
		<div class="container-fluid header-votacion">
			<div class="container-fluid">
				<div class="row">
					<div class="space50-padding"></div>
					<div class="col s12 m2 offset-m1 l2 offset-l1 15-img">
						<a href="<?php echo base_url(); ?>">
							<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/SB_Logo15_Hor.svg">
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col s12 m12 l12 open-sans-light centered">
						<div class="space40"></div>
						<p class="titulo-finalistas font34">
							¡La votación terminó!
						</p>
						<p class="titulo-finalistas font20">
							Estas y todas las historias que recibimos, son el resultado de 15 años juntos.<br class="hide-on-small-only">
							Ustedes decidieron y estas son las historias ganadoras.
						</p>
						<p class="titulo-finalistas font34">
							¡Muchas felicidades!
						</p>
					</div>
				</div>
			</div>
		</div>
		
		<!--Videos Ganadores-->
		<div class="container ganadores">
			<div class="row">
				<div class="col s6 m6 l6">
					<div class="texto-pos-gan">
						<h2 class="open-sans-bold font30 centered">
							1er <span class="open-sans-light">Lugar
						</h2>
						<p class="open-sans-light font16 centered ganadores-viaje">
							<br>Viaje Doble a Seattle
						</p>
					</div>
					<div class="ganadores">
						<div class="space20"></div>
						<img class="responsive-img ganadores-img" src="<?php echo base_url(); ?>assets/img/01.jpg">
						<div class="space20"></div>
						<div class="row open-sans-regular vot-box">
							<div class="col s8 m7 l8">
								<p class="font19" id="name">
									Gisela Rivera
								</p>
								<p class="font12 lavanda" id="state">
									Estado de México
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col s6 m6 l6">
					<div class="texto-pos-gan">
						<h2 class="open-sans-bold font30 centered">
							2do <span class="open-sans-light">Lugar
						</h2>
						<p class="open-sans-light font16 centered ganadores-viaje">
							<br>Viaje Doble a Costa Rica
						</p>
					</div>
					<div class="ganadores">
						<div class="space20"></div>
						<img class="responsive-img ganadores-img" src="<?php echo base_url(); ?>assets/img/02.jpg">
						<div class="space20"></div>
						<div class="row open-sans-regular vot-box">
							<div class="col s8 m7 l8">
								<p class="font19" id="name">
									Laura Belmar
								</p>
								<p class="font12 lavanda" id="state">
									Estado de México
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row tercer-lugar">
				<div class="col s2 m4 l4 linea-pos">
					<div class="linea-ganadopres"></div>
				</div>
				<div class="col s8 m4 l4 titulo-tercer">
					<p class="open-sans-light font30 centered">
						3er-15° Lugar
					</p>
					<p class="open-sans-light font16 centered">
						Viaje Doble a Chiapas
					</p>
				</div>
				<div class="col s2 m4 l4 linea-pos">
					<div class="linea-ganadopres"></div>
				</div>
			</div>
		</div>
		<div class="container-fluid tres-quince">
			<div class="row">
				<div class="col s12 m10 offset-m1 l10 offset-l1 centered">
					<div class="cont-gan-chiapas">
						<div class="img-circular-contenedor">
							<img class="pos-img-centrado" src="<?php echo base_url(); ?>assets/img/03.jpg">
						</div>
						<p class="open-sans-light font14 centered participante-ganador">
							Ángeles UB
						</p>
						<p class="open-sans-light font12 centered participante-ciudad">
							Jalisco
						</p>
					</div>
					<div class="cont-gan-chiapas">
						<div class="img-circular-contenedor">
							<img class="pos-img-centrado" src="<?php echo base_url(); ?>assets/img/04.jpg">
						</div>
						<p class="open-sans-light font14 centered participante-ganador">
							Moisés Sánchez
						</p>
						<p class="open-sans-light font12 centered participante-ciudad">
							Ciudad de México
						</p>
					</div>
					<div class="cont-gan-chiapas">
						<div class="img-circular-contenedor">
							<img class="pos-img-centrado" src="<?php echo base_url(); ?>assets/img/05.jpg">
						</div>
						<p class="open-sans-light font14 centered participante-ganador">
							Arturo Ayala
						</p>
						<p class="open-sans-light font12 centered participante-ciudad">
							Puebla
						</p>
					</div>
					<div class="cont-gan-chiapas">
						<div class="img-circular-contenedor">
							<img class="pos-img-centrado" src="<?php echo base_url(); ?>assets/img/06.jpg">
						</div>
						<p class="open-sans-light font14 centered participante-ganador">
							José Padrón
						</p>
						<p class="open-sans-light font12 centered participante-ciudad">
							Estado de México
						</p>
					</div>
					<div class="cont-gan-chiapas">
						<div class="img-circular-contenedor">
							<img class="pos-img-centrado" src="<?php echo base_url(); ?>assets/img/07.jpg">
						</div>
						<p class="open-sans-light font14 centered participante-ganador">
							Mariana Mora
						</p>
						<p class="open-sans-light font12 centered participante-ciudad">
							Ciudad de México
						</p>
					</div>
					<div class="cont-gan-chiapas">
						<div class="img-circular-contenedor">
							<img class="pos-img-centrado" src="<?php echo base_url(); ?>assets/img/08.jpg">
						</div>
						<p class="open-sans-light font14 centered participante-ganador">
							Miriam Espino
						</p>
						<p class="open-sans-light font12 centered participante-ciudad">
							Baja California Sur
						</p>
					</div>
					<div class="cont-gan-chiapas">
						<div class="img-circular-contenedor">
							<img class="pos-img-centrado" src="<?php echo base_url(); ?>assets/img/09.jpg">
						</div>
						<p class="open-sans-light font14 centered participante-ganador">
							Pamela Rodríguez
						</p>
						<p class="open-sans-light font12 centered participante-ciudad">
							Nuevo León
						</p>
					</div>
					<div class="cont-gan-chiapas">
						<div class="img-circular-contenedor">
							<img class="pos-img-centrado" src="<?php echo base_url(); ?>assets/img/10.jpg">
						</div>
						<p class="open-sans-light font14 centered participante-ganador">
							Germán Calderón
						</p>
						<p class="open-sans-light font12 centered participante-ciudad">
							Puebla
						</p>
					</div>
					<div class="cont-gan-chiapas">
						<div class="img-circular-contenedor">
							<img class="pos-img-centrado" src="<?php echo base_url(); ?>assets/img/11.jpg">
						</div>
						<p class="open-sans-light font14 centered participante-ganador">
							Christian Canabal
						</p>
						<p class="open-sans-light font12 centered participante-ciudad">
							Yucatán
						</p>
					</div>
					<div class="cont-gan-chiapas">
						<div class="img-circular-contenedor">
							<img class="pos-img-centrado" src="<?php echo base_url(); ?>assets/img/12.jpg">
						</div>
						<p class="open-sans-light font14 centered participante-ganador">
							Jesús Morales
						</p>
						<p class="open-sans-light font12 centered participante-ciudad">
							Nuevo León
						</p>
					</div>
					<div class="cont-gan-chiapas">
						<div class="img-circular-contenedor">
							<img class="pos-img-centrado" src="<?php echo base_url(); ?>assets/img/13.jpg">
						</div>
						<p class="open-sans-light font14 centered participante-ganador">
							Eimy Hernández
						</p>
						<p class="open-sans-light font12 centered participante-ciudad">
							Nuevo León
						</p>
					</div>
					<div class="cont-gan-chiapas">
						<div class="img-circular-contenedor">
							<img class="pos-img-centrado" src="<?php echo base_url(); ?>assets/img/14.jpg">
						</div>
						<p class="open-sans-light font14 centered participante-ganador">
							Salomón Shayo
						</p>
						<p class="open-sans-light font12 centered participante-ciudad">
							Ciudad de México
						</p>
					</div>
					<div class="cont-gan-chiapas">
						<div class="img-circular-contenedor">
							<img class="pos-img-centrado" src="<?php echo base_url(); ?>assets/img/15.jpg">
						</div>
						<p class="open-sans-light font14 centered participante-ganador">
							Juan Aguera
						</p>
						<p class="open-sans-light font12 centered participante-ciudad">
							Quintana Roo
						</p>
					</div>
				</div>
			</div>
			<div class="row">
			<div class="space60"></div>
			</div>
		</div>
		
