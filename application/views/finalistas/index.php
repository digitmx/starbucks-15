		
		<!--Banner Header Finalistas-->
		<div class="container-fluid header-votacion finalistas-header">
			<div class="container-fluid">
				<div class="row">
					<div class="space50-padding"></div>
					<div class="col s12 m2 offset-m1 l2 offset-l1 15-img">
						<a href="<?php echo base_url(); ?>">
							<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/SB_Logo15_Hor.svg">
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col s12 m12 l12 open-sans-light centered">
						<div class="space40"></div>
						<p class="titulo-finalistas font34">
							Conoce cada una de las historias finalistas<br class="hide-on-small-only">
							y vota por tu favorita.
						</p>
					</div>
				</div>
			</div>
		</div>
		
		<!--Videos Finalistas-->
		<div class="container-fluid votacion-video finalistas-video">
			<div class="row videos">
				<div class="col s6 m4 l4 centered edit-contenedor-finalistas">
					<div class="contenedor-sombra">
						<div class="background-ganador"></div>
						<div class="background-ganador-black"></div>
						<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/video-img1.png">
					</div>
					<div class="row open-sans-regular vot-box">
						<div class="col s12 m7 l6 offset-l1">
							<p class="font19">
								Laura González
							</p>
							<p class="font12 lavanda">
								Guadalajara, Jalisco.
							</p>
						</div>
						<div class="col s12 m4 l4">
							<form class="btn-vote">
								<button class="open-sans-bold" type="submit" name="action">Vota
								<img class="corazon-size" src="<?php echo base_url(); ?>assets/img/voto.svg">
								</button>
							</form>
						</div>
					</div>
				</div>
				<div class="col s6 m4 l4 centered edit-contenedor-finalistas">
					<div class="contenedor-sombra">
						<div class="background-ganador"></div>
						<div class="background-ganador-black"></div>
						<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/video-img2.png">
					</div>
					<div class="row open-sans-regular vot-box">
						<div class="col s12 m7 l6 offset-l1">
							<p class="font19">
								Laura González
							</p>
							<p class="font12 lavanda">
								Guadalajara, Jalisco.
							</p>
						</div>
						<div class="col s12 m4 l4">
							<form class="btn-vote">
								<button class="open-sans-bold" type="submit" name="action">Vota
								<img class="corazon-size" src="<?php echo base_url(); ?>assets/img/voto.svg">
								</button>
							</form>
						</div>
					</div>
				</div>
				<div class="col s6 m4 l4 centered edit-contenedor-finalistas">
					<div class="contenedor-sombra">
						<div class="background-ganador"></div>
						<div class="background-ganador-black"></div>
						<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/video-img3.png">
					</div>
					<div class="row open-sans-regular vot-box">
						<div class="col s12 m7 l6 offset-l1">
							<p class="font19">
								Laura González
							</p>
							<p class="font12 lavanda">
								Guadalajara, Jalisco.
							</p>
						</div>
						<div class="col s12 m4 l4">
							<form class="btn-vote">
								<button class="open-sans-bold" type="submit" name="action">Vota
								<img class="corazon-size" src="<?php echo base_url(); ?>assets/img/voto.svg">
								</button>
							</form>
						</div>
					</div>
				</div>
				<div class="col s6 m4 l4 centered edit-contenedor-finalistas">
					<div class="contenedor-sombra">
						<div class="background-ganador"></div>
						<div class="background-ganador-black"></div>
						<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/video-img4.png">
					</div>
					<div class="row open-sans-regular vot-box">
						<div class="col s12 m7 l6 offset-l1">
							<p class="font19">
								Laura González
							</p>
							<p class="font12 lavanda">
								Guadalajara, Jalisco.
							</p>
						</div>
						<div class="col s12 m4 l4">
							<form class="btn-vote">
								<button class="open-sans-bold" type="submit" name="action">Vota
								<img class="corazon-size" src="<?php echo base_url(); ?>assets/img/voto.svg">
								</button>
							</form>
						</div>
					</div>
				</div>
				<div class="col s6 m4 l4 centered edit-contenedor-finalistas">
					<div class="contenedor-sombra">
						<div class="background-ganador"></div>
						<div class="background-ganador-black"></div>
						<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/video-img5.png">
					</div>
					<div class="row open-sans-regular vot-box">
						<div class="col s12 m7 l6 offset-l1">
							<p class="font19">
								Laura González
							</p>
							<p class="font12 lavanda">
								Guadalajara, Jalisco.
							</p>
						</div>
						<div class="col s12 m4 l4">
							<form class="btn-vote">
								<button class="open-sans-bold" type="submit" name="action">Vota
								<img class="corazon-size" src="<?php echo base_url(); ?>assets/img/voto.svg">
								</button>
							</form>
						</div>
					</div>
				</div>
				<div class="col s6 m4 l4 centered edit-contenedor-finalistas">
					<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/video-img6.png">
					<div class="row open-sans-regular vot-box">
						<div class="col s12 m7 l6 offset-l1">
							<p class="font19">
								Laura González
							</p>
							<p class="font12 lavanda">
								Guadalajara, Jalisco.
							</p>
						</div>
						<div class="col s12 m4 l4">
							<form class="btn-vote">
								<button class="open-sans-bold" type="submit" name="action">Vota
								<img class="corazon-size" src="<?php echo base_url(); ?>assets/img/voto.svg">
								</button>
							</form>
						</div>
					</div>
				</div>
				<div class="col s6 m4 l4 centered edit-contenedor-finalistas">
					<div class="contenedor-sombra">
						<div class="background-ganador"></div>
						<div class="background-ganador-black"></div>
						<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/video-img3.png">
					</div>
					<div class="row open-sans-regular vot-box">
						<div class="col s12 m7 l6 offset-l1">
							<p class="font19">
								Laura González
							</p>
							<p class="font12 lavanda">
								Guadalajara, Jalisco.
							</p>
						</div>
						<div class="col s12 m4 l4">
							<form class="btn-vote">
								<button class="open-sans-bold" type="submit" name="action">Vota
								<img class="corazon-size" src="<?php echo base_url(); ?>assets/img/voto.svg">
								</button>
							</form>
						</div>
					</div>
				</div>
				<div class="col s6 m4 l4 centered edit-contenedor-finalistas">
					<div class="contenedor-sombra">
						<div class="background-ganador"></div>
						<div class="background-ganador-black"></div>
						<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/video-img7.png">
					</div>
					<div class="row open-sans-regular vot-box">
						<div class="col s12 m7 l6 offset-l1">
							<p class="font19">
								Laura González
							</p>
							<p class="font12 lavanda">
								Guadalajara, Jalisco.
							</p>
						</div>
						<div class="col s12 m4 l4">
							<form class="btn-vote">
								<button class="open-sans-bold" type="submit" name="action">Vota
								<img class="corazon-size" src="<?php echo base_url(); ?>assets/img/voto.svg">
								</button>
							</form>
						</div>
					</div>
				</div>
				<div class="col s6 m4 l4 centered edit-contenedor-finalistas">
					<div class="contenedor-sombra">
						<div class="background-ganador"></div>
						<div class="background-ganador-black"></div>
						<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/video-img1.png">
					</div>
					<div class="row open-sans-regular vot-box">
						<div class="col s12 m7 l6 offset-l1">
							<p class="font19">
								Laura González
							</p>
							<p class="font12 lavanda">
								Guadalajara, Jalisco.
							</p>
						</div>
						<div class="col s12 m4 l4">
							<form class="btn-vote">
								<button class="open-sans-bold" type="submit" name="action">Vota
								<img class="corazon-size" src="<?php echo base_url(); ?>assets/img/voto.svg">
								</button>
							</form>
						</div>
					</div>
				</div>
				<div class="col s6 m4 l4 centered edit-contenedor-finalistas">
					<div class="contenedor-sombra">
						<div class="background-ganador"></div>
						<div class="background-ganador-black"></div>
						<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/video-img1.png">
					</div>
					<div class="row open-sans-regular vot-box">
						<div class="col s12 m7 l6 offset-l1">
							<p class="font19">
								Laura González
							</p>
							<p class="font12 lavanda">
								Guadalajara, Jalisco.
							</p>
						</div>
						<div class="col s12 m4 l4">
							<form class="btn-vote">
								<button class="open-sans-bold" type="submit" name="action">Vota
								<img class="corazon-size" src="<?php echo base_url(); ?>assets/img/voto.svg">
								</button>
							</form>
						</div>
					</div>
				</div>
				<div class="col s6 m4 l4 centered edit-contenedor-finalistas">
					<div class="contenedor-sombra">
						<div class="background-ganador"></div>
						<div class="background-ganador-black"></div>
						<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/video-img2.png">
					</div>
					<div class="row open-sans-regular vot-box">
						<div class="col s12 m7 l6 offset-l1">
							<p class="font19">
								Laura González
							</p>
							<p class="font12 lavanda">
								Guadalajara, Jalisco.
							</p>
						</div>
						<div class="col s12 m4 l4">
							<form class="btn-vote">
								<button class="open-sans-bold" type="submit" name="action">Vota
								<img class="corazon-size" src="<?php echo base_url(); ?>assets/img/voto.svg">
								</button>
							</form>
						</div>
					</div>
				</div>
				<div class="col s6 m4 l4 centered edit-contenedor-finalistas">
					<div class="contenedor-sombra">
						<div class="background-ganador"></div>
						<div class="background-ganador-black"></div>
						<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/video-img3.png">
					</div>
					<div class="row open-sans-regular vot-box">
						<div class="col s12 m7 l6 offset-l1">
							<p class="font19">
								Laura González
							</p>
							<p class="font12 lavanda">
								Guadalajara, Jalisco.
							</p>
						</div>
						<div class="col s12 m4 l4">
							<form class="btn-vote">
								<button class="open-sans-bold" type="submit" name="action">Vota
								<img class="corazon-size" src="<?php echo base_url(); ?>assets/img/voto.svg">
								</button>
							</form>
						</div>
					</div>
				</div>
				<div class="col s6 m4 l4 centered edit-contenedor-finalistas">
					<div class="contenedor-sombra">
						<div class="background-ganador"></div>
						<div class="background-ganador-black"></div>
						<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/video-img4.png">
					</div>
					<div class="row open-sans-regular vot-box">
						<div class="col s12 m7 l6 offset-l1">
							<p class="font19">
								Laura González
							</p>
							<p class="font12 lavanda">
								Guadalajara, Jalisco.
							</p>
						</div>
						<div class="col s12 m4 l4">
							<form class="btn-vote">
								<button class="open-sans-bold" type="submit" name="action">Vota
								<img class="corazon-size" src="<?php echo base_url(); ?>assets/img/voto.svg">
								</button>
							</form>
						</div>
					</div>
				</div>
				<div class="col s6 m4 l4 centered edit-contenedor-finalistas">
					<div class="contenedor-sombra">
						<div class="background-ganador"></div>
						<div class="background-ganador-black"></div>
						<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/video-img5.png">
					</div>
					<div class="row open-sans-regular vot-box">
						<div class="col s12 m7 l6 offset-l1">
							<p class="font19">
								Laura González
							</p>
							<p class="font12 lavanda">
								Guadalajara, Jalisco.
							</p>
						</div>
						<div class="col s12 m4 l4">
							<form class="btn-vote">
								<button class="open-sans-bold" type="submit" name="action">Vota
								<img class="corazon-size" src="<?php echo base_url(); ?>assets/img/voto.svg">
								</button>
							</form>
						</div>
					</div>
				</div>
				<div class="col s6 m4 l4 centered edit-contenedor-finalistas">
					<div class="contenedor-sombra">
						<div class="background-ganador"></div>
						<div class="background-ganador-black"></div>
						<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/video-img6.png">
					</div>
					<div class="row open-sans-regular vot-box">
						<div class="col s12 m7 l6 offset-l1">
							<p class="font19">
								Laura González
							</p>
							<p class="font12 lavanda">
								Guadalajara, Jalisco.
							</p>
						</div>
						<div class="col s12 m4 l4">
							<form class="btn-vote">
								<button class="open-sans-bold" type="submit" name="action">Vota
								<img class="corazon-size" src="<?php echo base_url(); ?>assets/img/voto.svg">
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="row no-margin-row">
				<div class="col s12 m12 l12 open-sans-light centered">
					<div class="space20"></div>
					<p class="font20 redes-sociales finalistas-redes white-text">
						Compártela con tus amigos en
						<a href="#" class="fb-btn">
							<img src="<?php echo base_url(); ?>assets/img/fb_share.png">
						</a>
						<span class="barra-redes finalistas-redes font30 white-text">|</span>
						<a href="#" class="tw-btn">
							<img src="<?php echo base_url(); ?>assets/img/tw_share.png">
						</a>
					</p>
					<div class="space100"></div>
				</div>
			</div>
		</div>