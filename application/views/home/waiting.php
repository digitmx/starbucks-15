		
		<!--Banner Header Home-->
		<div class="container-fluid header-home">
			<div class="container-fluid">
				<div id="videoBlock" class="hide-on-med-and-down">
					<video preload="preload"  id="video" autoplay="autoplay" loop="loop" class="fillWidth">
						<source src="<?php echo base_url(); ?>assets/video/home/Home_bg_1280x720.mp4" type="video/mp4"></source>
					</video>
					<div id="videoMessage" style="height: auto !important;">
						<div class="row">
							<div class="space40"></div>
							<div class="space150-padding"></div>
							<div class="col s5 m5 offset-m2 l12 15-img">
								<a href="<?php echo base_url(); ?>">
									<img src="<?php echo base_url(); ?>assets/img/SB_Logo15_Hor.svg">
								</a>
							</div>
						</div>
						<div class="row" id="compartenos">
							<div class="col s5 m5 offset-m2 l6 offset-l3 centered open-sans-light">
								<h2 class="font60">
									¡Muchas gracias por votar!
								</h2>
								<p class="font20">
								Nos encantó recordar juntos tus historias favoritas con Starbucks.
								</p>
								<p class="font20">
									Pronto anunciaremos a los ganadores del concurso en redes sociales.
								</p>
							</div>
						</div>
						<div class="space20"></div>
						<div class="row" id="cta">
							<div class="col s5 m5 offset-m2 l6 offset-l3 centered open-sans-light">
								<div class="open-sans-semibold font22 centered boton-cta">
									<a href="<?php echo base_url(); ?>premios">Ver premios</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="hero-mobile" class="hide-on-large-only">
					<div class="row">
						<div class="col s12 15-img">
							<div class="space40"></div>
							<img src="<?php echo base_url(); ?>assets/img/SB_Logo15_Hor.svg">
						</div>
					</div>
					<div class="row">
						<div class="col s10 offset-s1 m10 offset-m1 centered open-sans-light">
							<div class="space100"></div>
							<h2 class="font60">
								¡Muchas gracias por votar!
							</h2>
							<p class="font20">
								Nos encantó recordar juntos tus historias favoritas con Starbucks.
							</p>
							<p class="font20">
								Pronto anunciaremos a los ganadores del concurso en redes sociales.
							</p>
						</div>
					</div>
					<div class="space20"></div>
					<div class="row" id="cta">
						<div class="col s12 centered open-sans-light">
							<div class="open-sans-semibold font22 centered boton-cta">
								<a href="<?php echo base_url(); ?>premios">Ver premios</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>