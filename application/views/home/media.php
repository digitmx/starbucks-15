
		<!--Banner Header Media-->
		<div class="container-fluid">
			<div class="container-fluid">
				<div class="row">
					<div class="space150-padding"></div>
					<div class="col s12 m12 l12 15-img">
						<a href="<?php echo base_url(); ?>">
							<img src="<?php echo base_url(); ?>assets/img/SB_Logo15_Hor.svg">
						</a>
					</div>
					<div class="space150-padding"></div>
				</div>
			</div>
		</div>
		
		<!--Seccion Formulario Media-->
		<div class="container">
			<form id="formMedia" enctype="multipart/form-data" name="formMedia" accept-charset="utf-8" method="post" action="<?php echo base_url(); ?>proccess/add_media">
				<input type="hidden" id="iduser" name="iduser" value="<?php echo $data['iduser']; ?>" />
				<div class="row">
					<div class="col s12 m10 offset-m1 l8 offset-l2">
						<p class="open-sans-light font30 centered">
							¿Quieres tener más oportunidades de ganar?
						</p>
						<p class="open-sans-light font22 centered">
							Compártenos fotos o videos de tu historia.
						</p>
					</div>
				</div>
				<div class="space30"></div>
				<div class="row">
					<div class="col s6 m6 l6">
						<div class="file-field input-field">
							<div class="btn fotos-video-btn">
								<input type="file" id="images" name="images[]" multiple>
								<img class="img-foto-video" src="<?php echo base_url(); ?>assets/img/Icono_fotos.svg">
								<p class="open-sans-light font22 centered">
									Subir álbum de fotos
								</p>
								<p class="open-sans-light font16 centered">
									(máx. 3 fotos)
								</p>
							</div>
							<div class="file-path-wrapper media-placeholder hide">
								<input class="file-path validate open-sans-light font16 centered" type="text" placeholder="(máx. 3 fotos)">
							</div>
						</div>
					</div>
					<div class="col s6 m6 l6">
						<div class="file-field input-field">
							<div class="btn fotos-video-btn">
								<input type="file" id="video" name="video">
								<img class="img-foto-video" src="<?php echo base_url(); ?>assets/img/Icono_video.svg">
								<p class="open-sans-light font22 centered">
									Subir video
								</p>
								<p class="open-sans-light font16 centered">
									(máx. 30s, 20mb)
								</p>
							</div>
							<div class="file-path-wrapper media-placeholder hide">
								<input class="file-path validate open-sans-light font16 centered" type="text" placeholder="(máx. 3 fotos)">
							</div>
						</div>
					</div>
				</div>
				<div class="space80"></div>
				<div class="row">
					<div class="col s12 m10 offset-m1 l8 offset-l2">
						<p class="open-sans-light font30 centered">
							¿Eres miembro de <span class="open-sans-bold">My Starbucks Rewards?</span>
						</p>
						<p class="open-sans-light font22 centered">
							Escribe el correo con el que te registraste al programa o el número de tu tarjeta My Starbucks Rewards
						</p>
					</div>
				</div>
				<div class="space30"></div>
				<div class="row email-media">
					<div class="col s10 offset-s1 m10 offset-m1 l10 offset-l1">
						<input class="browser-default" type="text" autocomplete="off" id="inputSB" name="inputSB">
					</div>
					<div class="col s12 m12 l12">
						<div class="space20"></div>
						<p class="open-sans-light font14 centered">
							* No soy miembro pero me interesa obtener <a target="_blank" href="https://rewards.starbucks.mx/LoyaltyWeb/HowWorks" class="gold">más información<a>
						</p>
					</div>
				</div>
				<div class="space60"></div>
				<div class="row">
					<div class="col s12 open-sans-semibold font26 centered boton-enviar">
						<input type="submit" id="btnFormMedia" name="btnFormMedia" value="ENVIAR">
					</div>
				</div>
				<div class="space150-padding"></div>
			</form>
		</div>