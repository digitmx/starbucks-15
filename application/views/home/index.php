		
		<!--Banner Header Home-->
		<div class="container-fluid header-home">
			<div class="container-fluid">
				<div id="videoBlock" class="hide-on-med-and-down">
					<video preload="preload"  id="video" autoplay="autoplay" loop="loop" class="fillWidth">
						<source src="<?php echo base_url(); ?>assets/video/home/Home_bg_1280x720.mp4" type="video/mp4"></source>
					</video>
					<div id="videoMessage">
						<div class="row">
							<div class="space40"></div>
							<div class="space150-padding"></div>
							<div class="col s5 m5 offset-m2 l12 15-img">
								<a href="<?php echo base_url(); ?>">
									<img src="<?php echo base_url(); ?>assets/img/SB_Logo15_Hor.svg">
								</a>
							</div>
						</div>
						<div class="row" id="compartenos">
							<div class="col s5 m5 offset-m2 l6 offset-l3 centered open-sans-light">
								<h2 class="font60">
									Compártenos tu historia.
								</h2>
								<p class="font20">
									Cumplimos <span class="open-sans-bold">15 años en México</span> y queremos recordar todas esas historias que nos unen.
								</p>
							</div>
						</div>
						<div class="space20"></div>
						<div class="row" id="cta">
							<div class="col s5 m5 offset-m2 l6 offset-l3 centered open-sans-light">
								<div class="open-sans-semibold font22 centered boton-cta">
									<a href="#formulario">Regístrala aquí</a>
								</div>
							</div>
						</div>
						<div class="row" id="arrow">
							<div class="col s12 centered chevron-home">
								<a href="#formulario">
									<img src="<?php echo base_url(); ?>assets/img/chevron.png">
								</a>
							</div>
						</div>
					</div>
				</div>
				<div id="hero-mobile" class="hide-on-large-only">
					<div class="row">
						<div class="col s12 15-img">
							<div class="space40"></div>
							<img src="<?php echo base_url(); ?>assets/img/SB_Logo15_Hor.svg">
						</div>
					</div>
					<div class="row">
						<div class="col s10 offset-s1 m10 offset-m1 centered open-sans-light">
							<div class="space100"></div>
							<h2 class="font60">
								Compártenos tu historia.
							</h2>
							<p class="font20">
								Cumplimos <span class="open-sans-bold">15 años en México</span> y queremos recordar todas esas historias que nos unen.
							</p>
						</div>
					</div>
					<div class="space20"></div>
					<div class="row" id="cta">
						<div class="col s12 centered open-sans-light">
							<div class="open-sans-semibold font22 centered boton-cta">
								<a href="#formulario">Regístrala aquí</a>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col s12 centered chevron-home">
							<a href="#formulario">
								<img src="<?php echo base_url(); ?>assets/img/chevron.png">
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!--Seccion Formulario-->
		<div class="container" id="formulario">
			<div class="row formulario-edit">
				<div class="col s12 m10 offset-m1 l8 offset-l2">
					<div class="space40"></div>
					<p class="open-sans-light font22 centered">
						¡Manda la mejor historia que hayas vivido con <span class="open-sans-bold">Starbucks!</span> Podrás ganar un viaje doble a Seattle para conocer el Starbucks Reserve Roastery & Tasting Room y más premios increíbles. 
					</p>
					<div class="space20"></div>
					<p class="open-sans-light font22 centered">
						Conoce los detalles de los <a href="<?php echo base_url(); ?>premios" class="gold"><b>premios</b></a> aquí.
					</p>
					<div class="space40"></div>
					<p class="open-sans-semibold font26 centered">
						<a href="#modal-video" class="gold bold underline modal-trigger">Conoce cómo participar</a>
					</p>
					<div id="modal-video" class="modal transparent">
						<div class="modal-header transparent">
					    	<a href="#!" class="right font22 modal-action modal-close waves-effect btn-flat white-text bold">X</a>
					    </div>
					    <div class="modal-content">
					    	<div class="col s12">
								<div class="video-container">
									<div id="player"></div>
									<!--<iframe width="560" height="315" src="https://www.youtube.com/embed/rHepWgWP6ZE?autoplay=0&controls=0&fs=0&iv_load_policy=3&modestbranding=1&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>-->
									<script>
								      // 2. This code loads the IFrame Player API code asynchronously.
								      var tag = document.createElement('script');
								
								      tag.src = "https://www.youtube.com/iframe_api";
								      var firstScriptTag = document.getElementsByTagName('script')[0];
								      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
								
								      // 3. This function creates an <iframe> (and YouTube player)
								      //    after the API code downloads.
								      var player;
								      function onYouTubeIframeAPIReady() {
								        player = new YT.Player('player', {
								          height: '360',
								          width: '640',
								          videoId: 'rHepWgWP6ZE',
								          playerVars: {
										      'autoplay': 0,
									          'controls': 0,
									          'fs': 0,
									          'iv_load_policy': 3,
									          'modestbranding': 1,
									          'rel': 0,
									          'showinfo': 0
									      },
								          events: {
								            'onReady': onPlayerReady,
								            'onStateChange': onPlayerStateChange
								          }
								        });
								      }
								
								      // 4. The API will call this function when the video player is ready.
								      function onPlayerReady(event) {
								      	//event.target.playVideo();
								      }
								
								      // 5. The API calls this function when the player's state changes.
								      //    The function indicates that when playing a video (state=1),
								      //    the player should play for six seconds and then stop.
								      var done = false;
								      function onPlayerStateChange(event) {
									      if (event.data == 1)
									      {
										  	ga('send', 'event', 'Videos', 'Reproducir - Starbucks México, 15 años juntos.', 'Video home');
										  	console.log('ga');
									      }
								      }
								      function stopVideo() {
								        player.stopVideo();
								      }
								    </script>
								</div>
							</div>
					    </div>
					</div>
					<br />
				</div>
				<div class="row">
					<div class="col s12 open-sans-semibold font22 centered boton-enviar-f">
						<div class="space40"></div>
						<a href="#">INICIAR SESIÓN CON FACEBOOK</a>
					</div>
				</div>
				<form id="formHistory" enctype="multipart/form-data" name="formHistory" accept-charset="utf-8" method="post" action="<?php echo base_url(); ?>proccess/create_history">
					<div class="col s12 m10 offset-m1 l8 offset-l2 open-sans-light font22 formulario-pos">
						<div class="row">
							<div class="col s12 m12 l6">
								<input class="browser-default block" autocomplete="off" id="inputName" name="inputName" type="text" value="<?=(isset($facebook['name'])) ? $facebook['name'] : ''; ?>" placeholder="Nombre">
								<br/>
							</div>
							<div class="col s12 m12 l6">
								<input class="browser-default block" autocomplete="off" id="inputLastname" name="inputLastname" type="text" value="<?=(isset($facebook['lastname'])) ? $facebook['lastname'] : ''; ?>" placeholder="Apellido">
								<br/>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l6">
								<select class="browser-default block" id="inputGenre" name="inputGenre" >
									<option value="" disabled selected>Selecciona un Género</option>
									<option value="Femenino">Femenino</option>
									<option value="Masculino">Masculino</option>
									<option value="Prefiero no decirlo">Prefiero no decirlo</option>
    							</select>
								<br/>
							</div>
							<div class="col s12 m12 l6">
								<input class="browser-default block" autocomplete="off" id="inputEmail" name="inputEmail" type="text" value="<?=(isset($facebook['email'])) ? $facebook['email'] : ''; ?>" placeholder="Correo electrónico">
								<br/>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l6">
								<select class="browser-default block" id="inputState" name="inputState" >
									<option value="" disabled selected>Selecciona un Estado</option>
									<option value="Aguascalientes">Aguascalientes</option>
									<option value="Baja California">Baja California</option>
									<option value="Baja California Sur">Baja California Sur</option>
									<option value="Campeche">Campeche</option>
									<option value="Chiapas">Chiapas</option>
									<option value="Chihuahua">Chihuahua</option>
									<option value="Ciudad de México">Ciudad de México</option>
									<option value="Coahuila">Coahuila</option>
									<option value="Colima">Colima</option>
									<option value="Durango">Durango</option>
									<option value="Estado de México">Estado de México</option>
									<option value="Guanajuato">Guanajuato</option>
									<option value="Guerrero">Guerrero</option>
									<option value="Hidalgo">Hidalgo</option>
									<option value="Jalisco">Jalisco</option>
									<option value="Michoacán">Michoacán</option>
									<option value="Morelos">Morelos</option>
									<option value="Nayarit">Nayarit</option>
									<option value="Nuevo León">Nuevo León</option>
									<option value="Oaxaca">Oaxaca</option>
									<option value="Puebla">Puebla</option>
									<option value="Querétaro">Querétaro</option>
									<option value="Quintana Roo">Quintana Roo</option>
									<option value="San Luis Potosí">San Luis Potosí</option>
									<option value="Sinaloa">Sinaloa</option>
									<option value="Sonora">Sonora</option>
									<option value="Tabasco">Tabasco</option>
									<option value="Tamaulipas">Tamaulipas</option>
									<option value="Tlaxcala">Tlaxcala</option>
									<option value="Veracruz">Veracruz</option>
									<option value="Yucatán">Yucatán</option>
									<option value="Zacatecas">Zacatecas</option>
								</select>
								<br/>
							</div>
							<div class="col s12 m12 l6">
								<input class="browser-default block" autocomplete="off" id="inputPhone" name="inputPhone" type="text" placeholder="Teléfono">
								<br/>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12">
								<input class="browser-default bebida-fav block" autocomplete="off" id="inputProduct" name="inputProduct" type="text" placeholder="¿Cuál es tu bebida favorita?">
								<br />
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 centered">
								<p class="open-sans-light font22 centered">
									Relátanos cronológicamente tu historia con <span class="open-sans-bold">Starbucks</span>, incluyendo el año en el que sucedió.
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<div class="row">
									<div class="input-field col s12">
										<textarea id="inputHistory" name="inputHistory" class="browser-default block textForm"></textarea>
										<label for="inputHistory"></label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 centered">
								<p class="open-sans-light font22 centered">
									¿Cuál fue el rol de <span class="open-sans-bold">Starbucks</span> en tu historia y qué efecto tuvo esta historia en tu vida?
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<div class="row">
									<div class="input-field col s12">
										<textarea id="inputRole" name="inputRole" class="browser-default block textForm"></textarea>
										<label for="inputRole"></label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12">
								<p class="open-sans-light font22 centered">
									¿Quieres tener más oportunidades de ganar?
								</p>
								<p class="open-sans-light font16 centered">
									Compártenos fotos o videos de tu historia.
								</p>
							</div>
						</div>
						<div class="space30"></div>
						<div class="row">
							<div class="col s12 m12 l6">
								<div class="file-field input-field">
									<div class="btn fotos-btn">
										<!--<input type="file" id="images" name="images[]" multiple>-->
										<input type="hidden" id="image_size" name="image_size" value="0" />
										<img class="img-foto-video" src="<?php echo base_url(); ?>assets/img/Icono_fotos.svg">
										<span class="open-sans-light font22 centered">
											Subir <span class="fix">álbum de </span>fotos
										</span>
										<p class="open-sans-light font16 centered">
											(máx. 10mb)
										</p>
									</div>
									<div class="file-path-wrapper media-placeholder hide">
										<input class="file-path validate open-sans-light font16 centered" type="text" placeholder="(máx. 3 fotos)">
									</div>
								</div>
								<div class="file-images-list"></div>
							</div>
							<div class="col s12 m12 l6">
								<div class="file-field input-field">
									<div class="btn fotos-video-btn">
										<!--<input type="file" id="video" name="video">-->
										<input type="hidden" id="video_counter" name="video_counter" value="0" />
										<img class="img-foto-video" src="<?php echo base_url(); ?>assets/img/Icono_video.svg">
										<span class="open-sans-light font22 centered">
											Subir video
										</span>
										<p class="open-sans-light font16 centered">
											(máx. 30s, 100mb)
										</p>
									</div>
									<div class="file-path-wrapper media-placeholder hide">
										<input class="file-path validate open-sans-light font16 centered" type="text" placeholder="(máx. 3 fotos)">
									</div>
								</div>
								<div class="file-video-list"></div>
							</div>
						</div>
						<div class="space40"></div>
						<div class="row sbrewards">
							<div class="col s12 m12 l12">
								<p class="open-sans-light font22 centered" id="title">
									¿Eres miembro de <span class="open-sans-bold">My Starbucks Rewards?</span>
								</p>
								<p class="open-sans-light font16 centered" id="subtitle">
									Escribe el número de tu tarjeta My Starbucks Rewards <i href="#sbcard" class="modal-trigger material-icons info">info_outline</i>
								</p>
								<div id="sbcard" class="modal transparent">
									<div class="modal-header transparent">
								    	<a href="#!" class="right font22 modal-action modal-close waves-effect btn-flat white-text bold">X</a>
								    </div>
								    <div class="modal-content">
								    	<div class="centered">
											<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/tarjeta.png">
										</div>
								    </div>
								</div>
							</div>
						</div>
						<div class="space30"></div>
						<div class="row email-media">
							<div class="col s12 m12 l12">
								<div class="row">
									<div class="col s3">
										<input class="browser-default" maxlength="4" type="text" autocomplete="off" id="inputSB1" placeholder="0000" name="inputSB1">
									</div>
									<div class="col s3">
										<input class="browser-default" maxlength="4" type="text" autocomplete="off" id="inputSB2" placeholder="0000" name="inputSB2">
									</div>
									<div class="col s3">
										<input class="browser-default" maxlength="4" type="text" autocomplete="off" id="inputSB3" placeholder="0000" name="inputSB3">
									</div>
									<div class="col s3">
										<input class="browser-default" maxlength="4" type="text" autocomplete="off" id="inputSB4" placeholder="0000" name="inputSB4">
									</div>
								</div>
							</div>
							<div class="col s12 m12 l12">
								<p class="open-sans-light font14 centered">
									No soy miembro pero me interesa obtener <a target="_blank" href="https://rewards.starbucks.mx/LoyaltyWeb/HowWorks" class="gold">más información.</a>
								</p>
							</div>
						</div>
						<div class="space60"></div>
						<div class="row">
							<div class="col s12 m12 l6 centered"
								<p>
									<input type="checkbox" class="filled-in terminos" id="inputTerms" name="inputTerms" />
									<label for="inputTerms" class="font14">He leído y acepto los Términos y Condiciones</label>
								</p>
							</div>
							<div class="col s12 m12 l6 centered"
								<p>
									<input type="checkbox" class="filled-in privacidad" id="inputPrivacy" name="inputPrivacy" />
									<label for="inputPrivacy" class="font14">Acepto el Aviso de Privacidad</label>
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col s12 open-sans-semibold font22 centered boton-enviar">
								<div class="space20"></div>
								<a href="#" id="btnFormHistory" name="btnFormHistory">ENVIAR</a>
								<div class="space60"></div>
								<span class="block font16">Sólo es posible hacer un registro por persona.</span>
								<div class="space40"></div>
								<?php if ($this->session->userdata("errorFormHistory")) { echo $this->session->userdata("errorFormHistory")['msg']; } ?>
								<?php $this->session->sess_destroy(); ?>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>