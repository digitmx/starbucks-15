		
		<!--Banner Header Votacion-->
		<div class="container-fluid header-votacion">
			<div class="container-fluid">
				<div class="row">
					<!--<div class="space150-padding"></div>-->
					<div class="space40"></div>
					<div class="col s12 m12 l12 15-img">
						<a href="<?php echo base_url(); ?>">
							<img src="<?php echo base_url(); ?>assets/img/SB_Logo15_Hor.svg">
						</a>
					</div>
				</div>
				<div class="row parrafo-votacion">
					<div class="col s12 m12 l8 offset-l2 open-sans-light centered">
						<h2 class="font60">
							Conoce cada una de las historias finalistas y vota por tu favorita.
						</h2>
						<!--<div class="space80"></div>-->
						<!--<p class="font20">
							Elige a las historias que ganarán tres grandes experiencias en Starbucks.
						</p>-->
					</div>
				</div>
				<!--<div class="row votacion-chv">
					<div class="col s12 m12 l12 centered">
						<img src="<?php echo base_url(); ?>assets/img/chevron.png">
					</div>
				</div>-->
			</div>
		</div>
		
		<!--Videos Votacion-->
		<div class="container-fluid votacion-video">
			<div class="row videos">
				<?php foreach ($data as $row) { if (isset($row['youtube'])) { $youtube_id = $row['youtube']; } else { $youtube_id = 'rHepWgWP6ZE'; } ?>
				<?php $name = explode(' ', $row['name']); ?>
				<?php $lastname = explode(' ', $row['lastname']); ?>
				<?php $nombre = $name[0] . ' ' . $lastname[0]; ?>
				<div class="col s12 m12 l4 centered">
					<div class="video-container">
						<div data-id="<?php echo $youtube_id; ?>" data-name="<?php echo $nombre; ?>"></div>
					</div>
					<div class="row open-sans-regular vot-box">
						<div class="col s8 m7 l8">
							<p class="font19" id="name">
								<?php echo $nombre; ?>
							</p>
							<p class="font12 lavanda" id="state">
								<?php echo $row['state']; ?>
							</p>
						</div>
						<div class="col s4 m4 l4">
							<form class="btn-vote right-align" rel="<?php echo $nombre; ?>" id="formVote" name="formVote" method="post" action="<?php echo base_url(); ?>proccess/vote">
								<button class="open-sans-bold" type="submit" name="action">Vota
									<img class="corazon-size" src="<?php echo base_url(); ?>assets/img/voto.svg">
								</button>
								<input type="hidden" id="iduser" name="iduser" value="<?php echo $row['iduser']; ?>">
								<input type="hidden" id="ip" name="ip" value="<?php echo $ip; ?>" />
							</form>
							<p class="font12 white-text right-align" id="votes">
								<?php echo $row['votes']; ?> votos
							</p>
						</div>
					</div>
					<div class="space20"></div>
				</div>
				<?php } ?>
				<script>
				    // 2. This code loads the IFrame Player API code asynchronously.
				    var tag = document.createElement('script');
				
				    tag.src = "https://www.youtube.com/iframe_api";
				    var firstScriptTag = document.getElementsByTagName('script')[0];
				    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
				
				    // 3. This function creates an <iframe> (and YouTube player)
				    //    after the API code downloads.
				    var player;
					function onYouTubeIframeAPIReady() {
						var players = document.querySelectorAll('.video-container div')
					    for (var i = 0; i < players.length; i++) {
					        new YT.Player(players[i], {
						        height: '360',
						        width: '640',
					            playerVars: {
					                'autoplay': 0,
							        'controls': 1,
							        'fs': 1,
							        'iv_load_policy': 3,
							        'modestbranding': 1,
							        'rel': 0,
							        'showinfo': 0
					            },
					            name: players[i].dataset.name,
					            videoId: players[i].dataset.id,
					            events: {
						        	'onReady': onPlayerReady,
									'onStateChange': onPlayerStateChange
						        }
					        });
					    }
				    }
				
				    // 4. The API will call this function when the video player is ready.
				    function onPlayerReady(event) {
				    	//event.target.playVideo();
				    }
				
				    // 5. The API calls this function when the player's state changes.
				    //    The function indicates that when playing a video (state=1),
				    //    the player should play for six seconds and then stop.
				    var done = false;
				    function onPlayerStateChange(event) {
					    if (event.data == 1)
					    {
						    //ga('send', 'event', 'Votaciones', 'Votar por historia', 'Test');
							console.log(event);
					    }
				    }
				    
				    function stopVideo() {
				        player.stopVideo();
				    }
				    
				</script>
			</div>
		</div>
		
		<div class="space40"></div>
		<center>
			<p class="font16 white-text redes-sociales open-sans-regular">
				Compártela con tus amigos en
				<br class="hide-on-med-and-up"><br class="hide-on-med-and-up">
				<a href="#" class="fb-btn btnShareFacebook">
					<img src="<?php echo base_url(); ?>assets/img/fb_share.png">
				</a>
				<span class="barra-redes font30">|</span>
				<a href="#" class="tw-btn btnShareTwitter">
					<img src="<?php echo base_url(); ?>assets/img/tw_share.png">
				</a>
			</p>
		</center>
		<div class="space100"></div>