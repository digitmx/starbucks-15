		
		<!--Gracias Votacion-->
		<div class="container-fluid gracias-votacion">
			<div class="container-fluid">
				<div class="row">
					<div class="space50-padding"></div>
					<div class="col s12 m12 l12 15-img">
						<a href="<?php echo base_url(); ?>">
							<img src="<?php echo base_url(); ?>assets/img/SB_Logo15_Hor.svg">
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col s12 m12 l12 open-sans-light centered">
						<div class="space60"></div>
						<p class="font32">
							Tu voto fue registrado.
						</p>
						<p class="font60 parrafo-esp3px">
							¿Te gustó esta historia?
						</p>
						<p class="font20 redes-sociales white-text">
							Compártela con tus amigos en
							<br class="hide-on-med-and-up"><br class="hide-on-med-and-up">
							<a href="#" class="fb-btn btnShareFacebook">
								<img src="<?php echo base_url(); ?>assets/img/fb_share.png">
							</a>
							<span class="barra-redes font30 white-text">|</span>
							<a href="#" class="tw-btn btnShareTwitter">
								<img src="<?php echo base_url(); ?>assets/img/tw_share.png">
							</a>
						</p>
						<div class="space40"></div>
						<p class="font20">
							Recuerda que la votación termina el 15 de octubre de 2017.
						</p>
						<div class="space80"></div>
						<a href="<?php echo base_url(); ?>" class="btn-gracias">
							<p class="font20">
								REGRESAR
							</p>
						</a>
					</div>
				</div>
				<div class="row no-margin-row">
					<div class="space100"></div>
				</div>
			</div>
		</div>