		
		<!--Terminos y Condiciones-->
		<div class="container-fluid terminos">
			<div class="container">
				<div class="row">
					<div class="space20"></div>
					<div class="col s12 m12 l4 15-img">
						<a href="<?php echo base_url(); ?>">
							<img class="responsive-img" src="<?php echo base_url(); ?>assets/img/SB_Logo15_Hor.svg">
						</a>
					</div>
					<div class="col s12 m12 l8">
						<h2 class="open-sans-light font32 terminos-titulo">
							Términos y Condiciones.
						</h2>
						<div class="space40"></div>
					</div>
				</div>
				<div class="row no-margin-row terminos-condiciones">
					<div class="col s12 m12 l8 right texto-terminos" >
						<article class="ter-con-edit">
							<h2 class="open-sans-bold font24 centered">BASES LEGALES DEL CONCURSO<br>“STARBUCKS CELEBRA”</h2><br>
							<p class="open-sans-light font14">
								<strong>1. DESCRIPCIÓN DEL CONCURSO.</strong> La promoción, en la modalidad de concurso, denominada “Starbucks Celebra” (el “Concurso”) comienza el 1 de septiembre de 2017 a las 10:00 horas y termina el 15 de octubre de 2017 a las 23:59 horas (en adelante, el “Período de Participación”), sujeto a los periodos y etapas establecidas en el apartado “Mecánica de Participación” y a las presentes Bases. Este Concurso es organizado por BLOK MEDIA S.C. (en adelante, “BLOK MEDIA”) en representación de Café Sirena, S. de R.L. de C.V. (en adelante STARBUCKS). Para los propósitos de estos términos y condiciones todos los horarios son del huso horario de la Ciudad de México.
								<br><br>
								Como parte de la celebración de los 15 años de Starbucks en México, queremos reconocer y compartir las historias que nuestros clientes han vivido con la marca, sus tiendas, productos y empleados.
								<br><br>
								No se requerirá compra, consumo o adquisición de producto o servicio alguno para participar. 
								<br><br>
								<strong>Mecánica de Participación:</strong> Del 1 de septiembre de 2017 a las 10:00 horas y hasta el 1 de octubre de 2017 a las 23:59 horas, los participantes podrán ingresar a la página web <a href="<?php echo base_url(); ?>" class="m-gold">www.starbucks15.mx</a>, desde un navegador de internet, para registrar una historia que hayan vivido con STARBUCKS durante los 15 años de su presencia en México, conforme a lo señalado en la sección 4.1. (Etapa de Registro). 
								<br><br>
								Una vez que se cierre la Etapa de Registro, un jurado formado y seleccionado por STARBUCKS, elegirá las quince (15) mejores historias que serán finalistas y pasarán a la Etapa de Votación, conforme a lo señalado en la sección 4.2. (Etapa de Jurado). 
								<br><br>
								Posteriormente, el 4 de octubre de 2017 a las 10:00 horas, las 15 historias elegidas como finalistas serán publicadas dentro del sitio web <a href="<?php echo base_url(); ?>" class="m-gold">www.starbucks15.mx</a> y del 4 de octubre de 2017 a las 10:01 horas y hasta  el 15 de octubre de 2017 a las 23:59 horas, se abrirá la etapa de votación pública, conforme a la mecánica señalada en la sección 4.3., en la cual se definirá el ranking de ganadores del Concurso (Etapa de Votación). 
								<br><br>
								Los 15 ganadores de la Etapa de Votación serán acreedores a los premios descritos en la sección 5, dependiendo de la cantidad de votos que reciban.
								<br><br>
								<strong>2. QUIÉN PUEDE PARTICIPAR.</strong> El Concurso solamente está abierto para personas físicas mayores de 18 años, que sean residentes legales de México. Los participantes deben ser mayores de 18 años de edad en el momento de inscribirse para poder participar, deberán contar con una cuenta personal activa de correo o de Facebook (en adelante, la “Cuenta”) y contar con visa americana y pasaporte vigente al momento de inscripción y con vigencia suficiente para viajar dentro de los tiempos mencionados en la sección 5 PREMIOS y de acuerdo con las regulaciones internacionales de cada país.
								<br><br>
								No podrán participar en el Concurso los empleados y representantes de Café Sirena S de RL de C.V., o sus afiliados, subsidiarias, grupos controladores, o cualquier empresa y/o empleados o entidad directamente vinculada al desarrollo, implementación o publicidad de este Concurso, ni sus respectivos familiares inmediatos (padres, hijos, hermanos y cónyuges, independientemente de donde los mismos residan o tengan su domicilio) ni otros miembros que compartan su domicilio (las personas que compartan la misma residencia al menos tres (3) meses al año).
								<br><br>
								<strong>3.CÓMO PARTICIPAR.</strong> A fin de participar en el Concurso, los interesados deberán ingresar durante la Etapa de Registro al Sitio Web <a href="<?php echo base_url(); ?>" class="m-gold">www.starbucks15.mx</a> (en adelante, el “Sitio Web”) y registrarse desde su Cuenta de Facebook o con su correo electrónico. Posteriormente, los participantes deberán responder todas las preguntas y requisitos del formulario.
								<br><br>
								3.1. Información Personal:
								<br><br>
								<p class="open-sans-light font14">
									<div class="edit-list-none">
									<ul class="browser-default open-sans-light font14">
										<li>i.&nbsp;&nbsp;Los participantes son los responsables por asegurar que sus datos sean correctos para poder contactarlos. El contacto se realizará a través de los datos que el participante registre, desde el formulario o desde su cuenta de Facebook, por lo que es exclusiva responsabilidad de los participantes que los datos sean correctos y mantener activas y vigentes dichas cuentas.
										<br><br>
										<li>ii.&nbsp;&nbsp;La información personal entregada por los participantes como parte de este Concurso será utilizada para administrar el Concurso, seleccionar al o los ganadores de los  premios y, cumplir con ellos.
										<br><br>
										<li>iii.&nbsp;&nbsp;La falta de inclusión de alguno o de todos los datos hará perder a la persona registrada el derecho a participar en el Concurso, asimismo, el suministro de información falsa o inexacta impedirá la participación en el Concurso. Será responsabilidad exclusiva de los Participantes, la correcta entrada de sus datos para registrarse en el Concurso. Una vez leídas y entendidas las Bases Legales, deberán en su caso, aceptar las mismas presionando el botón “He leído y Acepto los Términos y Condiciones” así como el “Aviso de Privacidad” el cual puede ser consultado en <a href="<?php echo base_url(); ?>avisodeprivacidad" class="m-gold">www.starbucks15.mx/avisodeprivacidad.</a>
										<br><br>
										<li>iv.&nbsp;&nbsp;Una vez que los Participantes lleven a cabo su registro correctamente en el Concurso, éstos quedarán habilitados para participar en el mismo.
										<br><br>
										IMPORTANTE: Sólo se admitirá 1 (una) inscripción por Participante, lo que significa que cualquier intento de o comisión de fraude en cualquier instancia del Concurso, será causa suficiente para la descalificación de dicho Participante.
										</li>
										<br><br>
										<li>v.&nbsp;&nbsp;Las personas que participan en este Concurso, por su sola participación y aceptación expresa de las Bases del Concurso, autorizan a STARBUCKS, por tiempo indefinido y de manera irrevocable, a difundir su imagen y voz, así como nombres, domicilio, testimonios, experiencias y biografías (en adelante “Imagen”), conforme a lo siguiente:
										<br><br>
										La Imagen podrá ser utilizada por STARBUCKS en todo tipo de publicación impresa o electrónica del Concurso, así como en cualquier tipo de medio de difusión o comunicación pública en cualquier formato que exista en la actualidad o que llegue a existir en el futuro, incluyendo sin limitación su uso en medios físicos y/o digitales, así como redes sociales, para fines comerciales y publicitarios, sin limites territoriales.
										<br><br>
										La autorización que en este acto se le otorga a STARBUCKS, podrá ser ejercida en México o en cualquier otra parte del mundo, directamente por dicha persona o por terceras personas que ella autorice para dichos efectos.
										<br><br>
										En el uso de la Imagen, STARBUCKS podrá utilizarla de forma estática, en movimiento, de forma aislada o en conjunto con otras imágenes, retratos, ilustraciones, imágenes, marcas, signos distintivos y/o con obras artísticas o literarias. Asimismo, STARBUCKS tendrá el derecho de llevar a cabo modificaciones en cuanto a tamaño o disposición y de instrumentar procedimientos de edición que tengan por objeto ampliarla, retocarla, editarla así como implementar doblajes o substitución de la voz o incorporar subtitulado de la misma en cualquier idioma.
										<br><br>
										STARBUCKS podrá utilizar el nombre del participante asociándolo a su Imagen, sin embargo, no usará  de manera que se afecte su reputación o imagen pública.
										<br><br>
										De igual forma, STARBUCKS podrá asociar el nombre e imagen del participante al nombre y marcas de STARBUCKS, filiales, subsidiarias y cualquier tercero y socio comercial.
										<br><br>
										La autorización que en este acto confiere y otorga voluntaria y expresamente el Participante es de naturaleza gratuita y por lo tanto, se obliga a no efectuar reclamación económica alguna por cualquier concepto relacionado con la autorización de uso de su Imagen que en este acto se encuentra confiriendo así como respecto de cualquier contraprestación económica que en un futuro le pudiese llegar a corresponder con motivo de lo anterior.
										</li>
										<br>
										<li>vi.&nbsp;&nbsp;La participación en este Concurso implica consentimiento para que los datos personales de los participantes integren las bases de datos de STARBUCKS, como así también autorización tanto para el tratamiento automatizado de dichos datos o información, y para su utilización en relación con la actividad comercial que desarrolle el Organizador, y/o cualquiera de sus subsidiarias o afiliadas. Para mayor información los participantes podrán visitar la Política de Privacidad de Facebook y de STARBUCKS, que puede ser consultada en <a href="<?php echo base_url(); ?>avisodeprivacidad" class="m-gold">www.starbucks15.mx/avisodeprivacidad.</a>
										<br><br>
										<li>vii.&nbsp;&nbsp;El derecho de uso de la imagen y voz, así como nombres, domicilio, testimonios, experiencias y biografías (en adelante “Imagen”) de los participantes, ampara tanto la información entregada durante la realización del Concurso como la entregada durante la entrega de premios y la realización de los viajes.
									</ul>
									</div>
								</p>
								<br>
								<p class="open-sans-light font14">
								<strong>4. MECÁNICA DE PARTICIPACIÓN.</strong>
								<br><br>
								El Concurso “Starbucks 15 años” que se encontrará ubicado en la siguiente dirección web: <a href="<?php echo base_url(); ?>" class="m-gold">www.starbucks15.mx</a>, incluirá dos secciones:
								<br><br>
								La primera denominada “Registra tu Historia” será un formulario para:
								<br>
								</p>
								<p class="open-sans-light font14">
									<ul class="browser-default open-sans-light font14">
										<li>Registrar los datos de los participantes</li>
										<li>Registrar sus historias</li>
										<li>Adjuntar materiales adicionales como fotos o video</li>
									</ul>
								</p>
								<p class="open-sans-light font14">
								La segunda, denominada “Vota” presentará una recopilación de las 15 historias finalistas para permitir la votación y la selección de los ganadores, en la Etapa de Votación.
								<br><br>
								La participación en el Concurso implica el conocimiento y aceptación de las presentes bases, mecánica, términos y condiciones establecidas en el presente documento. Es indispensable estar de acuerdo con los términos y condiciones establecidos en las Bases del Concurso. Su aceptación implica regirse por las mismas para el desarrollo del presente Concurso. Las políticas, mecánica y restricciones no son apelables, ni están sujetas a negociación o modificación de ninguna índole. BLOK MEDIA se reserva el derecho de hacer cualquier tipo de cambio a las presentes Bases sin previo aviso, por lo que el usuario se compromete a revisarlas periódicamente durante la Vigencia del Concurso. Por lo anterior, STARBUCKS se reserva el derecho de modificar, cambiar, cancelar, prolongar, disminuir, posponer o anular las vigencias, requisitos, términos y condiciones, así como los premios a entregar con motivo del Concurso, bastan solamente la mención en las Bases de dichos cambios, previa autorización escrita por parte de STARBUCKS, o si a su juicio existieran elementos que impidan su realización. 
								</p>
								<p class="open-sans-light font14">
								<strong>4.1 Registra tu Historia</strong>
								<br><br>
								4.1.1 Durante la Etapa de Registro, los participantes deberán registrar sus datos personales, tales  como nombre, apellido, género, correo electrónico, ciudad, teléfono y bebida favorita de “Starbucks”. Podrán elegir registrar sus datos manualmente o a través de su cuenta de Facebook.
								<br><br>
								4.1.2 Dentro del mismo formulario, los participantes deberán contestar obligatoriamente las tres siguientes preguntas presentadas para poder registrar una historia positiva que hayan vivido con STARBUCKS durante los 15 años de su presencia en México:
								</p>
								<p class="open-sans-light font14">
									<div class="edit-list-roman">
										<ul class="browser-default open-sans-light font14">
											<li>Relátanos cronológicamente tu historia con Starbucks incluyendo el año en el que sucedió:</li>
											<li>¿Cuál fue el rol de Starbucks (tiendas, partners, productos, etc) en tu historia?</li>
											<li>¿Qué efecto tuvo esta historia en tu vida?</li>
										</ul>
									</div>
								</p>
								<p class="open-sans-light font14">
								4.1.3. Los participantes podrán agregar materiales como fotografías o videos que complementen o ayuden a comprobar la veracidad de su historia. Asimismo podrán ingresar su cuenta de “My Starbucks Rewards” en caso de que sean miembros del programa de lealtad de “Starbucks”.
								<br><br>
								<strong>4.2 Elección de Finalistas</strong>
								<br><br>
								4.2.1 Al cierre de la Etapa de Registro, un jurado conformado y seleccionado por STARBUCKS, seleccionará como finalistas las quince (15) mejores historias del total de registros realizados en la página durante la Etapa de Registro. Los criterios de selección de las quince (15) historias ganadoras serán los siguientes:
								</p>
								<p class="open-sans-light font14">
									<ul class="browser-default open-sans-light font14">
										<li>Claridad. Las historias deberán contener la respuesta a las tres preguntas precisadas en estas bases de manera clara y concisa;</li>
										<li>Veracidad. Las historias deberán ser reales y sujetas a comprobación de veracidad a través de fotografías, videos o documentos formales que las acrediten.</li>
										<li>Grado de relación directa con Starbucks. Deberán demostrar el rol positivo o presencia que tuvo la marca, sus productos, tiendas o empleados dentro de la historia.</li>
										<li>Alineación con los valores de Starbucks. Deberán soportar o ejemplificar los valores de la marca:</li>
											<div class="edit-list-circle">
												<ul class="browser-default open-sans-light font14">
													<li>Crear una cultura de calidez y pertenencia, donde todos son bienvenidos</li>
													<li>Actuar con coraje, retar el status quo y encontrar nuevas formas de hacernos crecer entre nosotros y a nuestra compañía</li>
													<li>Estar en el presente, conectando con transparencia, dignidad y respeto</li>
													<li>Dar lo mejor de nosotros en todo lo que hacemos, siendo responsables de los resultados.</li>
												</ul>
											</div>
										<li>Territorio. Las historias tendrán que haber sucedido o tener relación con la presencia de la marca en la República Mexicana.</li>
									</ul>
								</p>
								<p class="open-sans-light font14">
								4.2.2 Una vez que sean seleccionadas las quince (15) historias finalistas en la Etapa de Jurado, de conformidad con los criterios antes señalados, los nombres de los o las finalistas se publicarán en la página <a href="<?php echo base_url(); ?>" class="m-gold">www.starbucks15.mx</a> y en sus redes sociales, a las 10:00 am el día 4 de octubre de 2017. A partir de ese momento, y hasta las 23:59 del 15 de octubre de 2017, se habilitará la Votación Pública dentro del sitio de <a href="<?php echo base_url(); ?>" class="m-gold">www.starbucks15.mx</a>.
								<br><br>
								4.2.3. Los participantes que no hayan resultado “finalistas”, conforme al presente proceso, se entenderán como “eliminados”, por lo que ya no serán acreedores ni tendrán posibilidad de obtener los Premios, por lo que reconocen que en caso de no resultar finalista, el participante no recibirá premio ni retribución alguno.
								<br><br>
								<strong>4.3 Votación Pública</strong>
								<br><br>
								Una vez que inice la Etapa de Votación, terceras personas ajenas a STARBUCKS podrán acceder libremente a la página web <a href="<?php echo base_url(); ?>" class="m-gold">www.starbucks15.mx</a> 
								<br><br>
								Dichas personas eligirán libremente la historia que más les guste, bajo los criteros libres de cada persona, y emitirán su voto por dicha persona.
								<br><br>
								Los votos podrán estar limitados a máximo 3 votos diarios por dirección IP.
								<br><br>
								Los finalistas no podrán realizar actos tendientes a aumentar la cantidad de votos, tales como compra de votos, competencia desleal, bots, o cualquier otro medio, tecnología o dispositivo que permita la votación  masiva y viole la seguridad de la página web.
								<br><br>
								<strong>4.4 Ganadores</strong>
								<br><br>
								4.4.1 La cantidad de votos que cada historia reciba, identificará el lugar del probable ganador dentro del Ranking y el premio al que será acreedor una vez cumplido el punto 4.2.2. En este sentido, habrá quince ganadores, divididos en primer, segundo, tercer, cuarto, quinto, sexto, séptimo, octavo, noveno, décimo, undécimo, duodécimo, décimo tercero, décimo cuarto y décimo quinto lugar, y habrá únicamente un premio por cada lugar.
								<br><br>
								Para efectos de interpretación, dentro de los 15 finalistas, se le asignará el premio dependiendo de la cantidad de votos en la Votación Pública, en comparación con los demás finalistas y el lugar obtenido, es decir, primer, segundo,  tercer lugar y así sucesivamente.
								<br><br>
								Los ganadores y sus respectivos premios, se publicarán en la página web <a href="<?php echo base_url(); ?>" class="m-gold">www.starbucks15.mx</a> y en las redes sociales de STARBUCKS, a las 12:00 horas el día 18 de octubre de 2017. 
								<br><br>
								4.4.2 Al final del Concurso, si el finalista resulta ser un probable ganador del mismo, deberá demostrar la veracidad de su historia para ser acreedor al premio de acuerdo a lo establecido en el punto 7. En caso de no comprobar la veracidad a satisfacción de STARBUCKS, el participante será descalificado y se contactará al siguiente participante que cumpla con las características mencionadas de los ganadores.
								<br><br>
								4.4.3 STARBUCKS no se hará responsable de los gastos de transportación y viáticos que reclamar su premio implique. Los ganadores deberán cubrir los gastos de transporte por su cuenta, y en caso de no poder asistir a recoger su premio, el premio será reasignado al siguiente finalista que tenga el mayor número de votos entre los finalistas restantes que no hubieren sido señalados como “ganadores”.
								<br><br>
								4.4.4 Los Participantes sólo pueden participar en el Concurso durante la Etapa de Registro y a través de su Cuenta de Facebook o por correo electrónico. Los participantes pueden inscribirse solo una vez durante la Etapa de Registro para participar en el Concurso. No se aceptará ningún otro método de participación que no sea el descrito en estas bases.
								<br><br>
								STARBUCKS no será responsable por pérdidas de información, interrupciones o incapacidad de la red, servidor, fallas en las transmisiones de las líneas telefónicas, fallas técnicas o fallas del Sitio Web, sean ellas provenientes de un error de tipo humano, mecánico o electrónico que pudieren afectar la participación en el Concurso o la recepción de la comunicación al Ganador. STARBUCKS se reserva el derecho a descalificar participaciones múltiples provenientes de la misma persona, o de una misma cuenta de Facebook realizadas mediante métodos de duplicación robóticos, automáticos, repetitivos, programados o con métodos de duplicación similares, y a descalificar a cualquier persona que utilice dichos métodos. Se considerará que las participaciones han sido realizadas  por el propietario autorizado de la Cuenta de Facebook o de correo electrónico utilizada para participar del Concurso.  Se entenderá por "propietario autorizado de la Cuenta de Facebook o de correo electrónico utilizada para participar del Concurso" a la persona  que administra la Cuenta de Facebook o correo electrónico utilizada para participar en el Concurso.
								<br><br>
								<strong>5. PREMIOS.</strong>
								<br><br>
								Los premios se otorgarán a los 15 ganadores, en sus respectivos lugares, a partir de la cantidad de votos recibidos a través del sitio <a href="<?php echo base_url(); ?>" class="m-gold">www.starbucks15.mx</a> al finalizar la Etapa de Votación. 
								<br><br>
								A manera de interpretación, de los 15 finalistas, los que reciban más votaciones en relación a los demás finalistas, independientemente de la cantidad de votos, será merecedores del premio en el respectivo lugar, es decir, no habrá ningun máximo ni minimo de votos.
								<br><br>
								Los premios se otorgarán de la siguiente forma:
								<br><br>
								5.1.1 Primer Lugar: Al usuario que registró la historia finalista que obtuvo la mayor cantidad de votos en el sitio web. Un viaje para dos personas a Seattle, EE.UU. que incluye lo siguiente.
								</p>
								<p class="open-sans-light font14">
									<ul class="browser-default open-sans-light font14">
										<li>Vuelo redondo, en clase turista y por linea aérea comercial a definir, para dos personas mayores de edad saliendo desde la Ciudad de México hacia Seattle, EE.UU. y de regreso a la Ciudad de México.</li>
										<li>Traslado terrestre privado desde el Aeropuerto de Seattle al Hotel y del Hotel al Aeropuerto.</li>
										<li>3 noches de hotel, en categoría comercial y de calidad de mínimo 3 estrellas y máximo 4, a elección de STARBUCKS, en una habitación doble.</li>
										<li>Visita a la primera tienda de STARBUCKS ubicada en 1912 Pike Pl, Seattle, WA 98101, EE.UU.</li>
										<li>Visita a Starbucks Reserve Roastery & Tasting Room, ubicada en 1124 Pike St, Seattle, WA 98101, EE.UU.</li>
										<li>Traslado terrestre privado de Hotel a Starbucks Pike y Starbucks Reserve Roastery & Tasting Room</li>
										<li>La fecha de viaje será durante noviembre o diciembre de 2017, a ser definido por STARBUCKS.</li>
										<li>No incluye: Alimentación, gastos de transporte no especificados, ni gastos personales o propinas.</li>
									</ul>
								</p>
								<p class="open-sans-light font14">
								5.1.2 Segundo Lugar: Al usuario que registró la historia finalista que obtuvo la segunda mayor cantidad de votos en el sitio web. Un viaje para dos personas a San José, Costa Rica que incluye lo siguiente.
								</p>
								<p class="open-sans-light font14">
									<ul class="browser-default open-sans-light font14">
										<li>Vuelo redondo, en clase turista y por linea aérea comercial a definir, para dos personas mayores de edad saliendo desde la Ciudad de México hacia San José, Costa Rica y de regreso a la Ciudad de México.</li>
										<li>Traslado terrestre privado desde el Aeropuerto de Costa Rica al lugar de Hospedaje y del Hospedaje al Aeropuerto.</li>
										<li>3 noches de hospedaje, a elección de STARBUCKS, en una habitación doble.</li>
										<li>Visita a la Hacienda Alsacia ubicada en las faldas del volcán Poás.</li>
										<li>Traslado terrestre privado de Hotel a la Hacienda Alsacia.</li>
										<li>La fecha de viaje será durante marzo de 2018, a ser definido por STARBUCKS.</li>
										<li>No incluye: Alimentación, gastos de transporte no especificados, ni gastos personales o propinas.</li>
									</ul>
								</p>
								<p class="open-sans-light font14">
								5.1.3 Tercer al Décimo Quinto Lugar: A los usuarios que registraron las historias finalistas que obtuvieron de la tercera mayor cantidad a la décimo quinta cantidad de votos en el sitio web. Un viaje para dos personas a San Cristóbal de las Casas, Chiapas que incluye lo siguiente.
								</p>
								<p class="open-sans-light font14">
									<ul class="browser-default open-sans-light font14">
										<li>Vuelo redondo, en clase turista y por linea aérea comercial a definir para dos personas mayores de edad saliendo desde la Ciudad de México hacia Tuxtla Gutiérrez, Chiapas y de regreso a la Ciudad de México.</li>
										<li>Traslado terrestre privado desde el Aeropuerto de Tuxtla Gutiérrez al Hotel y del Hotel al Aeropuerto.</li>
										<li>3 noches de hotel, en categoría comercial y de calidad de mínimo 3 estrellas y máximo 4, a elección de STARBUCKS,  en una habitación doble.</li>
										<li>Visita a una Finca de Café ubicada en San Cristobal de las Casas, Chiapas.</li>
										<li>Visita a Farmers Support Center en San Cristóbal de las Casas.</li>
										<li>Traslado terrestre privado de Hotel a la Finca  y Farmers Support Center en San Cristóbal de las Casas.</li>
										<li>La fecha de viaje será durante diciembre de 2017 o enero de 2018, a ser definido por STARBUCKS.</li>
										<li>No incluye: Alimentación, gastos de transporte no especificados, ni gastos personales o propinas.</li>
									</ul>
								</p>
								<p class="open-sans-light font14">
									Cualquier gasto adicional como por ejemplo, entre otros, transportes adicionales, cargos por servicios, excursiones opcionales, teléfono, compras en tiendas de regalos, impuestos, gastos médicos y cualquier otro costo no especificado expresamente en las presentes serán responsabilidad del Ganador. El Ganador y su acompañante están sujetos a las reglas y reglamentos de la aerolínea seleccionada por STARBUCKS. Es posible que los boletos de avión emitidos en relación con este Premio no sean elegibles para los programas de acumulación de millas de las aerolíneas y los mismos no pueden cambiarse. El Premio no puede usarse en combinación con ninguna otra oferta o promoción. Sólo pueden recibir y ejercer los premios personas mayores de 18 años.
									<br><br>
									STARBUCKS no es responsable si una experiencia debe cancelarse debido a condiciones climáticas o de seguridad, por lo que el Ganador se obliga a desistir de cualquier derecho de reclamación del premio que se llegue a suspender o cancelar por las condiciones antes marcadas.
									<br><br>
									Los premios no son transferibles. No se permiten sustituciones del Premio, excepto por parte de STARBUCKS en caso de causas ajenas y/o de fuerza mayor no imputable al mismo, previa autorización de la autoridad de aplicación. STARBUCKS no reemplazará ningún boleto de avión ni cheques de viajero que sean extraviados o robados, después de su entrega al Ganador. El Ganador es el único responsable de todos los gastos relacionados con la participación en este Concurso, incluyendo la aceptación de cualquier premio no especificado aquí. Todo ganador está sujeto a verificación por parte de STARBUCKS, cuyas decisiones son definitivas. Sólo STARBUCKS determinará la forma de la verificación. Un participante no es ganador de un premio hasta que su elegibilidad haya sido verificada, y el participante haya sido notificado que la verificación se haya completado.
									<br><br>
									Las probabilidades de ganar dependen del número de participantes que hayan participado del Concurso.
									<br><br>
									Los ganadores recibirán un correo electrónico por parte de STARBUCKS, una vez se haya publicado sus nombres en las redes sociales de STARBUCKS para la coordinación y logística del premio.
									<br><br>
									<strong>6. SELECCIÓN DEL PROBABLE GANADOR/NOTIFICACIÓN DEL GANADOR.</strong>
									<br><br>
									6.1. Una vez finalizada la Etapa de Votación, los finalistas que lideren el Ranking referido en el punto 4 precedente resultarán probables ganadores de los premios con el alcance para ello determinado en el punto 7 subsiguiente.
									<br><br>
									6.2. En caso de que una vez finalizada la Etapa de Votación, exista un empate entre dos o más finalistas, es decir que dos o más finalistas lideren el Ranking con el mismo puntaje, se entenderá como probables ganadores del Concurso a los finalistas que se hubiese registrado primero al Concurso.
									<br><br>
									6.3. Los Premios se anunciarán en el sitio web del Concurso <a href="<?php echo base_url(); ?>" class="m-gold">www.starbucks15.mx</a> desde el 1 de septiembre de 2017 a las 10:00 am. Los ganadores de los Premios se anunciarán en el sitio web del Concurso <a href="<?php echo base_url(); ?>" class="m-gold">www.starbucks15.mx</a> y en las redes sociales de STARBUCKS, Facebook <a href="http://www.facebook.com/starbucksmexico" class="m-gold">www.facebook.com/starbucksmexico</a>, Twitter <a href="http://www.twitter.com/starbucksmex" class="m-gold">www.twitter.com/starbucksmex</a>, <a href="http://www.instagram.com/starbucksmex" class="m-gold">www.instagram.com/starbucksmex</a>, el 18 de octubre de 2017 a las 12:00 horas.
									<br><br>
									<strong>7. RECLAMACIÓN DE PREMIOS.</strong> Finalizado el Período de Votación y dentro de las 24 horas de ser contactado por STARBUCKS, <span class="text-underline">será obligatorio que el probable ganador del Concurso y su acompañante envíe a STARBUCKS, vía correo electrónico o por fax, con una copia de su identificación, pasaporte y visa (en caso de ser aplicable), así como los elementos que prueban la veracidad de su historia, en caso  contrario perderá el derecho al Premio el cual será entregado a un probable Ganador Suplente,</span> quien será el participante ubicado en el puesto inmediatamente siguiente en el Ranking. Asimismo, será obligatorio que el probable Ganador del Premio llene, firme y devuelva una declaración jurada de elegibilidad, renuncia de responsabilidad y autorización para fines publicitarios, en un plazo de tres (3) días a partir de la recepción de la notificación por escrito del Premio. La declaración jurada de elegibilidad deberá ser remitida a STARBUCKS por fax o por servicio de courier a los números telefónicos y/o direcciones que oportunamente comunique STARBUCKS. El incumplimiento de este requisito en el plazo establecido, puede traer como consecuencia la anulación del Premio, el cual será adjudicado a un suplente, quien deberá cumplir con los mismos requisitos. También, será obligatorio que el acompañante de viaje del ganador del Premio llene, firme y devuelva una renuncia de responsabilidad y autorización para fines publicitarios, en un plazo de tres (3) días a partir de la recepción de la notificación por escrito del Premio vía servicio de courier prepagado de un día para otro y/o fax.
									<br><br>
									Si no es posible comunicarse vía correo electrónico con el probable ganador en el plazo de 48 horas, se le descalificará y se adjudicará el premio a su suplente, quien será el participante ubicado en el puesto inmediatamente siguiente en el Ranking, quien deberá cumplir con los mismos requisitos. STARBUCKS garantiza la entrega del Premio. El Premio será entregado por STARBUCKS a uno de los participantes que cumpla con los requisitos necesarios para resultar Ganador del Concurso. 
									<br><br>
									Al recibir el Premio del Concurso, el Ganador acepta su Premio sin que exista ninguna otra obligación por parte de STARBUCKS. El Ganador conviene en liberar y eximir de toda responsabilidad a STARBUCKS, sus compañías matrices, subsidiarias y afiliadas, así como a sus respectivos empleados y directivos, contratistas y agentes, y a cualquier otra persona u organización directamente involucrada con el Concurso, de toda responsabilidad en cualquier forma que pueda surgir, en su totalidad o en parte, directa o indirectamente, de su participación en el Concurso, incluyendo, entre otras, la pérdida, lesión, deceso u otro daño de cualquier tipo o de la aceptación, posesión o uso/uso indebido del Premio, por parte suya o por parte de cualquier otro receptor del Premio, que no sea el Ganador.
									<br><br>
									Asi mismo se descalificarán a los Participantes que incurran en algunos de los siguientes supuestos:
								</p>
								<p class="open-sans-light font14">
									<ul class="browser-default open-sans-light font14">
										<li>El incumplimiento de las presentes Bases Legales.</li>
										<li>Que haya participado en el concurso en 2 (dos) o más ocasiones con un mismo registro.</li>
										<li>Que el/la Participante haya vulnerado el sistema para obtener ventaja y/o más oportunidades por sobre los demás Participantes.</li>
										<li>Que el/la Participante no sea residente legal de los Estados Unidos Mexicanos.</li>
										<li>Que el/la Participante no cumpla con los requisitos establecidas en las presentes Bases Legales.</li>
										<li>Que el concursante realice actos tendientes a aumentar la cantidad de votos, tales como compra de votos, competencia desleal, bots, o cualquier otro medio, tecnología o dispositivo que permita la votación  masiva y viole la seguridad de la página web.</li>
										<li>Que el/la Participante y/o terceros relacionados al Participantes contravengan la moral y buenas costumbres respecto de comentarios u opiniones.</li>
									</ul>
								</p>
								<p class="open-sans-light font14">
								<strong>8. RENUNCIAS/LIMITACIÓN DE RESPONSABILIDAD.</strong> El Ganador acepta el uso de su nombre, imagen, voz y opiniones por parte de STARBUCKS y sus agentes, para publicidad, negocios o propósitos promocionales en cualquier medio y de cualquier manera, a nivel universal, sin compensación, consideración, notificación, ni aprobación alguna
								</p>
								<p class="open-sans-light font14">
									<ul class="browser-default open-sans-light font14">
										<li>&nbsp;&nbsp;Al inscribirse y participar en el Concurso, el participante en forma plena e incondicional conviene en aceptar y ser obligado por las condiciones de las presentes Bases legales, y por todas las decisiones de STARBUCKS, incluyendo, entre otras, las referentes a la elegibilidad, el ganador del Concurso y la interpretación de los términos utilizados en estas Bases legales, las cuales serán definitivas previa autorización de la autoridad de aplicación; y garantiza que toda la información proporcionada por él/ella en relación con el Concurso es verdadera, exacta y completa a la fecha de inicio para participar. Las decisiones de STARBUCKS con respecto a todos los asuntos relacionados con este Concurso son finales y vinculantes. STARBUCKS se reserva el derecho de modificar, cancelar, terminar o suspender este Concurso y a descalificar a cualquier individuo que viole los procedimientos de entrada, infrinja estos términos y condiciones, o actúe de manera disruptiva o inapropiada. Sin límite de lo anterior, STARBUCKS puede eliminar a un participante que, según el juicio STARBUCKS, haya sido descalificado, tenga una elegibilidad cuestionable o sea de otro modo no elegible para participar. Así mismo al aceptar los presentes Términos y Condiciones el Participante otorga su consentimiento expreso a STARBUCKS para recopilar y tratar sus datos personales únicamente para efectos de su participación en el presente Concurso, así como para la publicación de los ganadores del concurso en redes sociales y que conoce y acepta nuestro Aviso de Privacidad, el cual puede ser consultado en <a href="<?php echo base_url(); ?>avisodeprivacidad" class="m-gold">www.starbucks15.mx/avisodeprivacidad.</a></li>
									</ul>
								</p>
								<p class="open-sans-light font14">
									ADVERTENCIA: CUALQUIER PERSONA QUE INTENTE DELIBERADAMENTE SOCAVAR LA LEGITIMIDAD OPERATIVA DE ESTE CONCURSO, ALTERAR O DAÑAR UNA PÁGINA WEB SERÁ SUJETO A SANCIONES CIVILES, PENALES Y/O MULTAS SEGÚN CORRESPONDA. STARBUCKS SE RESERVA EL DERECHO A DEMANDAR POR DAÑOS Y PERJUICIOS A DICHA PERSONA (S), HASTA DONDE LA LEY LO PERMITA. STARBUCKS se reserva el derecho de modificar, cancelar, terminar o suspender este Concurso si, bajo la única opinión de STARBUCKS, en caso de que ocurra un incidente de cualquier tipo que interrumpa o corrompa la administración, seguridad, integridad o justicia del juego (tal como se planeó originalmente) de este Concurso, incluyendo de manera enunciativa más no limitativa a: (a) algún tipo de virus, intervención humana no autorizada, fraude, fallas técnicas u otras causas que corrompan o afecten la administración, seguridad, justicia, integridad o buena conducta del juego; o (b) terremotos, inundaciones, fuego, tormentas o cualquier otro desastre natural, caso de fuerza mayor, controversia laboral, perturbación civil o conmoción, disrupción de los mercados públicos, guerras o conflictos armados (ya sean o no, oficialmente declarados).
									<br><br>
									Al participar en este Concurso o al recibir un Premio, los participantes y en particular los ganadores, están de acuerdo en no causar daño a STARBUCKS y/o indemnizar a STARBUCKS y a cada una de sus respectivas compañías, a todos los oficiales de sus compañías, a directores, empleados y agentes (colectivamente, las "Entidades Descargadas") de cualquier responsabilidad, lesión, muerte, pérdida o daño que cualquier participante, entidad o persona, incluyendo de manera enunciativa más no limitativa : algún tipo de daño personal o en propiedad privada, causada en totalidad o en parte, directa o indirectamente, por su participación en este Concurso (o actividades relacionadas) o la aceptación, posesión, uso/o mal uso de un Premio (incluyendo cualquier actividad o viaje relacionado con el Premio).
									<br><br>
									En ningún caso, las Entidades Descargadas serán responsables de: (1) problemas o malfuncionamiento técnicos de cualquier clase, de cualquier red telefónica o de computación, ni de las líneas telefónicas, computadoras, servidores, o proveedores, equipos de computación, hardware o software, o (2) errores, interrupciones, defectos, demoras o fallos en las operaciones, incluyendo, entre otros, la transmisión inexacta de información de registración, o la no recepción por parte de STARBUCKS de ésta, debido a problemas técnicos o congestiones de tráfico en Internet o en cualquier sitio Web, o (3) interrupciones de la comunicación u otras fuerzas más allá del control razonable de STARBUCKS, incluyendo la incapacidad de obtener acceso al Sitio Web del Concurso, o cualquier interrupción relacionada con el tráfico en Internet, virus, “bugs”, o intervención desautorizada; (4) intervención humana no autorizada, eje., hacking; (5) error humano o técnico que ocurra en conexión con cualquier aspecto del Concurso: (6) cualquier daño o lesión a personas o propiedad privada que pueda ser causada, directa o indirectamente, en parte o en total, por la participación en el Concurso, acceso a, copiando o bajando materiales del Sitio Web o cualquier otro portal asociado al Concurso, o recibimiento o uso de cualquier premio.
									<br><br>
									Queda prohibido distribuir, modificar, transmitir, reenviar o utilizar el contenido del Portal con fines comerciales o públicos, incluyendo el texto, las imágenes, audio y/o videos allí contenidos.
									<br><br>
									Cada elemento que vea o lea en el Portal está protegido como derecho de propiedad intelectual -salvo que esté expresamente establecido lo contrario- por lo que no podrá utilizarla en forma diferente a lo señalado en estos términos y condiciones o en el resto del Portal, sin el previo y expreso permiso de STARBUCKS. STARBUCKS no garantizan ni prometen de ningún modo que el uso de la información exhibida en el Portal no violará derechos de terceras partes que no sean controladas o controlantes de y/o por STARBUCKS.
									<br><br>
									Aunque STARBUCKS ha efectuado y efectuará esfuerzos razonables para incluir información precisa y actualizada en el Portal, de ningún modo garantiza o promete que dicha información es absolutamente veraz. STARBUCKS no será responsable por ningún error u omisión contenidos en el Portal.
									<br><br>
									STARBUCKS no será responsable por ningún daño causado a su computadora o cualquier otro equipamiento por cualquier virus que pudiera infectarlos como consecuencia del acceso, uso o examen del Portal o como consecuencia de la transferencia que pudiera realizar a su computadora de cualquier archivo, dato, texto, imagen, video y/o audio contenidos en el Portal.
									<br><br>
									Nada de lo establecido y/o contenido en el Portal podrá ser interpretado como equivalente al otorgamiento, expreso o implícito, de una licencia o cualquier otro derecho de uso sobre alguna de las Marcas exhibidas en el Portal. El uso de las Marcas exhibidas en el Portal, está estrictamente prohibido. STARBUCKS hará respetar sus derechos de propiedad intelectual con el máximo rigor permitido por la ley, incluyendo el inicio de acciones penales.
									<br><br>
									STARBUCKS no ha revisado ninguno de los portales de Internet vinculados al Portal por lo que no será responsable por el contenido fuera del Portal o por cualquier otra liga vinculada al Portal. Su vinculación al Portal, páginas fuera del portal o cualquier otro portal es a su exclusivo riesgo y sin la intervención de STARBUCKS.
									<br><br>
									El uso o examen del Portal que pudiera realizar el Participante es a su exclusivo riesgo. Ni STARBUCKS, ni sus trabajadores o socios, ni cualquier otra parte involucrada en la operación, creación, producción o desarrollo del Portal será responsable por ninguna clase de daño y/o perjuicio -directo o indirecto o de cualquier otra clase- que hubieren sido ocasionados como consecuencia del acceso, uso o examen del Portal, incluso si STARBUCKS hubiera sido advertido de la posibilidad de tales pérdidas.
									<br><br>
									Sin perjuicio de lo expresado anteriormente, todo lo incluido en el Portal, está disponible "en la condición en la que se encuentra" sin garantías de ninguna especie expresas o implícitas. STARBUCKS no garantiza que el Portal satisfaga los requerimientos del Participante o que los servicios que en ella se ofrecen no sufran interrupciones, sean seguros o estén exentos de errores.
									<br><br>
									Está prohibido el envío o transmisión de material ilegal, amenazador, difamatorio, obsceno, escandaloso, provocativo, pornográfico, profano y/o de cualquier otra clase de material que pueda constituir o instigar la comisión de conductas que pudieran ser consideradas delito o que pudieran violar cualquier ley. STARBUCKS cooperará con cualquier autoridad que le solicite su colaboración para identificar a cualquiera que hubiere enviado información o material de dichas características.
									<br><br>
									STARBUCKS podrá en cualquier momento revisar estos términos y condiciones actualizando el Portal. El Participante está obligado a cumplir con cualquiera de esas modificaciones en caso de querer seguir utilizando el Portal y, por lo tanto, deberá visitar periódicamente el Portal para revisar los términos y condiciones actualizados que deberá cumplir.
									<br><br>
									El Participante será el único responsable de identificar y cumplir con todas las leyes de cualquier jurisdicción dentro y fuera de los Estados Unidos Mexicanos con relación al uso del Portal.
									<br><br>
									En algunos casos, se puede recopilar información técnica en forma automática (es decir, sin que medie el registro voluntario). Esta información incluye, por ejemplo, el tipo de explorador para Internet que utiliza el Participante, el sistema operativo de su computadora y el nombre de dominio desde el cual se vinculó con el Portal.
									<br><br>
									Cuando el Participante ingresa en el Portal, se puede almacenar alguna información en su computadora, bajo la forma de una "Cookie" o archivo similar. Con la mayoría de los exploradores para Internet, el Participante puede borrar las Cookies del disco duro de su computadora, bloquear todas las Cookies o recibir un mensaje de alerta antes de que se almacene una Cookie.
									<br><br>
									<stron>9. ACEPTACIÓN DE LAS BASES LEGALES.</stron> Los participantes de este Concurso aceptan que quedarán vinculados por estas bases legales y aceptan que: (a) las Entidades Descargadas y sus designados y asignados pueden usar (a menos que esté prohibido por ley) el nombre, la voz, la ciudad/estado de residencia, fotografías, clips de vídeo o película, y/o cualquier otra semejanza visual de la publicidad y/o del participante con fines comerciales y/o para cualquier otro propósito en cualquier medio y/o formato ahora o en el futuro, sin más compensación (financiera o de otro tipo ), permiso o notificación; y (b) las Entidades Descargadas y cada una de sus respectivas compañías matrices, subsidiarias, afiliados, funcionarios, directores, empleados, gobernadores, propietarios, distribuidores, minoristas, agentes, cesionarios, agencias de publicidad/promoción, representantes y agentes no tendrán responsabilidad y serán mantenidos a salvo de cualquier reclamo, acción, responsabilidad, pérdida, lesión o daño, incluyendo, sin limitación (pero sujeto a la operación de la ley), lesiones o la muerte del o los Ganadores o de terceros; o daños a bienes muebles o inmuebles debido en su totalidad o en parte, directa o indirectamente, por cualquier razón, incluyendo la aceptación, posesión, uso o mal uso del premio y/o la participación en este Concurso.
									<br><br> 
									<strong>10. BASES LEGALES DEL CONCURSO/NOMBRE DEL GANADOR.</strong> Puede obtenerse una copia de las presentes bases legales o el nombre del ganador del Concurso conectándose a: <a href="<?php echo base_url(); ?>terminosycondiciones" class="m-gold">www.starbucks15.mx/terminosycondiciones</a>
									<br><br>
									<strong>11. FACEBOOK.</strong> El Concurso es organizado exclusivamente por STARBUCKS. El Concurso no está patrocinado, avalado administrado ni asociado en modo alguno a Facebook. La información proporcionada por los participantes a fin de permitir la participación es otorgada a STARBUCKS, no a Facebook. Por su sola participación en el Concurso, los participantes reconocen que Facebook no ha tenido intervención alguna en la realización del Concurso, por lo que no le corresponde a Facebook responsabilidad alguna por cualquiera de las acciones contenidas en el presente Concurso.
									<br><br>
									POR FAVOR LEA DETENIDAMENTE LOS TÉRMINOS Y CONDICIONES ANTES DE SELECCIONAR LA CASILLA DE "HE LEÍDO Y ACEPTO LOS TÉRMINOS Y CONDICIONES" Y HACER SU REGISTRO. SELECCIONANDO LA CASILLA DE "HE LEÍDO Y ACEPTO LOS TÉRMINOS Y CONDICIONES" Y HACER SU REGISTRO, USTED ACUERDA QUEDAR OBLIGADO POR DICHOS TÉRMINOS Y CONDICIONES, DE CONFORMIDAD CON LO DISPUESTO POR LA FRACCIÓN I DEL ARTÍCULO 1803 DEL CÓDIGO CIVIL FEDERAL DE LOS ESTADOS UNIDOS MEXICANOS. SI USTED NO ESTÁ DE ACUERDO CON LOS MISMOS, OPRIMA EL BOTÓN "SALIR DE LA PÁGINA".
									<br><br>
									Responsable del Concurso BLOK MEDIA S.C. ubicado en Culiacán 123, Piso 4-401, Col. Hipódromo Condesa, Del. Cuauhtémoc, CP 06170, México, CDMX; para cualquier aclaración comunicarse a través de las redes sociales de STARBUCKS, Facebook <a href="http://www.facebook.com/starbucksmexico" class="m-gold">www.facebook.com/starbucksmexico</a>, Twitter <a href="http://www.twitter.com/starbucksmex" class="m-gold">www.twitter.com/starbucksmex</a> o <a href="http://www.instagram.com/starbucksmex" class="m-gold">www.instagram.com/starbucksmex</a>; o comunicarse de Lunes a Viernes de 8am a 8pm y Sábado y Domingo de 9am a 8pm al teléfono: <span class="m-gold">01800 288 0888</span> en la Ciudad de México y en el Interior de la República.
									<br><br>
								</p>
							</p>
						</article>
						<div class="space60"></div>
					</div>
					<div class="gradientback"></div>
				</div>
			</div>
		</div>