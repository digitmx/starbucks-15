		
		<!--Gracias Home-->
		<div class="container-fluid gracias-historia">
			<div class="container-fluid">
				<div class="row">
					<div class="space150-padding"></div>
					<div class="col s12 m12 l12 15-img">
						<a href="<?php echo base_url(); ?>">
							<img src="<?php echo base_url(); ?>assets/img/SB_Logo15_Hor.svg">
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col s12 m12 l12 open-sans-light centered">
						<div class="space60"></div>
						<p class="font32">
							Tu correo electrónico fue verificado.
						</p>
						<!--<p class="font60 parrafo-esp3px">
							¡Nos encantó recordarla contigo!
						</p>
						<p class="font20">
							Acabamos de enviarte un mail, te pedimos confirmes tu dirección de<br>
							correo electrónico para contactarte en caso de ser finalista. 
						</p>
						-->
						<p class="font16 white-text redes-sociales">
							Compartir
							<a href="#" class="fb-btn btnShareFacebook">
								<img src="<?php echo base_url(); ?>assets/img/fb_share.png">
							</a>
							<span class="barra-redes font30">|</span>
							<a href="#" class="tw-btn btnShareTwitter">
								<img src="<?php echo base_url(); ?>assets/img/tw_share.png">
							</a>
						</p>
						<div class="space60"></div>
						<a href="<?php echo base_url(); ?>premios" class="btn-gracias">
							<p class="font20">
								VER PREMIOS
							</p>
						</a>
					</div>
				</div>
				<div class="row no-margin-row">
					<div class="space100"></div>
				</div>
			</div>
		</div>