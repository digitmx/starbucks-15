		
		<!--Gracias Home-->
		<div class="container-fluid gracias-historia">
			<div class="container-fluid">
				<div class="row">
					<div class="space50-padding"></div>
					<div class="col s12 m12 l12 15-img">
						<a href="<?php echo base_url(); ?>">
							<img src="<?php echo base_url(); ?>assets/img/SB_Logo15_Hor.svg">
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col s12 m12 l12 open-sans-light centered">
						<div class="space40"></div>
						<p class="font32">
							Tu historia fue registrada.
						</p>
						<p class="font42 parrafo-esp3px">
							¡Nos encantó recordarla contigo!
						</p>
						<p class="font20">
							Te pedimos que nos ayudes confirmando tu dirección de<br class="hide-on-small-only">
							correo electrónico para contactarte en caso de que seas finalista.
						</p>
						<p class="font16 white-text redes-sociales">
							Compartir
							<a href="#" class="fb-btn btnShareFacebook">
								<img src="<?php echo base_url(); ?>assets/img/fb_share.png">
							</a>
							<span class="barra-redes font30">|</span>
							<a href="#" class="tw-btn btnShareTwitter">
								<img src="<?php echo base_url(); ?>assets/img/tw_share.png">
							</a>
						</p>
						<div class="space40"></div>
						<a href="<?php echo base_url(); ?>premios" class="btn-gracias">
							<p class="font20">
								VER PREMIOS
							</p>
						</a>
					</div>
				</div>
				<div class="row no-margin-row">
					<div class="space100"></div>
				</div>
			</div>
		</div>