		
		<!--Aviso Privacidad-->
		<div class="container-fluid terminos">
			<div class="container">
				<div class="row">
					<div class="space20"></div>
					<div class="col s12 m12 l4 15-img">
						<a href="<?php echo base_url(); ?>">
							<img src="<?php echo base_url(); ?>assets/img/SB_Logo15_Hor.svg">
						</a>
					</div>
					<div class="col s12 m12 l8">
						<h2 class="open-sans-light font32 terminos-titulo">
							Aviso de Privacidad
						</h2>
						<div class="space40"></div>
					</div>
				</div>
				<div class="row no-margin-row terminos-condiciones">
					<div class="col s12 m12 l8 right texto-terminos">
						<article class="articleBlock">
							<div class="articleBlock__text rtf">
								<h2 class="open-sans-bold font24">I. IDENTIDAD Y DOMICILIO DEL RESPONSABLE</h2>
								<p class="open-sans-light font14">El Responsable del tratamiento y protección de sus datos personales es Café Sirena S. de R.L. de C.V. <strong>(Starbucks Coffee®)</strong>, con domicilio en Avenida Revolución Número 1267, Torre Corporativa, Piso 21, Colonia Los Alpes, Delegación Álvaro Obregón, Código Postal 01040 en la Ciudad de México.</p>
								<p class="open-sans-light font14"><br></p>
								<h2 class="open-sans-bold font24">II. DATOS OBTENIDOS</h2>
								<p class="open-sans-light font14">Los datos personales que obtenemos en nuestro sitio web y Plataforma Digital (Mobile) son:&nbsp;</p>
									<p class="open-sans-light font14">
										<ul class="browser-default open-sans-light font14">
											<li>Nombre Completo.</li>
											<li>Género (sexo).</li>
											<li>Fecha de nacimiento.</li>
											<li>Domicilio (Estado, Delegación/Municipio, Código Postal).</li>
											<li>Teléfono fijo y/o Celular.</li>
											<li>Correo Electrónico.</li>
											<li>Registro Federal de Contribuyentes.</li>
											<li>Razón Social.</li>
											<li>Domicilio Fiscal.</li>
										</ul>
									</p>
								<p class="open-sans-light font14">Adicionalmente y en caso de que desee realizar recargas en línea a su tarjeta Starbucks Card o configurar recargas recurrentes, obtendremos los siguientes datos financieros en nuestra plataforma digital (Mobile):</p>
								<p class="open-sans-light font14">
									<ul class="browser-default open-sans-light font14">
										<li>Nombre completo del tarjetahabiente o titular de la tarjeta.</li>
										<li>Número de la tarjeta.</li>
										<li>Fecha de vencimiento.</li>
										<li>Tipo de tarjeta (Master Card/Visa/American Express).</li>
										<li>Código de Seguridad o la Información requerida por su banco o emisor de su tarjeta para la transacción o servicio en línea.</li>
									</ul>
									<p class="open-sans-light font14">
										Nosotros no recabamos datos personales sensibles en nuestro sitio web.
									</p>
								</p>
								<p class="open-sans-light font14"><br></p>
								<h2 class="open-sans-bold font24">III. FINALIDADES</h2>
								<p class="open-sans-light font14">Los datos personales obtenidos serán utilizados para las siguientes finalidades:</p>
								<p class="open-sans-light font14"><strong>Finalidades Primarias:</strong><br>
									<div class="edit-list">
										<ul class="browser-default open-sans-light font14">
											<li>Atender sus opiniones quejas y/ o sugerencias.</li>
											<li>Evaluar la calidad de nuestros servicios.</li>
											<li>Darle de alta en nuestro programa de lealtad My Starbucks Rewards.</li>
											<li>Brindarle el servicio de recarga para su tarjeta Starbucks Card, realizando los cargos correspondientes en las tarjetas bancarias para la transacción o el servicio adquirido por nuestros clientes.</li>
											<li>Poder enviarse su tarjeta Starbucks Gold Card a su domicilio.</li>
											<li>Realizar la facturación electrónica correspondiente de su consumo.</li>
											<li>La información personal recopilada a través del Sitio Web será utilizada para verificar la identidad de los usuarios, así como con fines analíticos, estadísticos y de mercadotecnia; lo anterior con la única finalidad de prestarle un mejor servicio.</li>
											<li>Realizar el trámite y respuesta de su Solicitud de Ejercicio de Derechos ARCO.</li>
											<li>Realizar su Solicitud de Revocación de Consentimiento o Negativa a Finalidades Secundarias.</li>
										</ul>
									</div>
								</p>
									<p class="open-sans-light font14"><strong>Finalidades Secundarias:</strong></p>
									<p class="open-sans-light font14">
										<div class="edit-list">
											<ul class="browser-default open-sans-light font14">
												<li>Envío de nuestras promociones y descuentos (publicidad y mercadotecnia).</li>
												<li>Realizar trivias, concursos y sorteos mediante redes sociales (Facebook, Twitter e Instagram).</li>
												<li>Poder identificarle y hacer entrega de premios a los ganadores de nuestros concursos y sorteos.</li>
												<li>Envío de felicitaciones y promociones el día de su cumpleaños.</li>
												<li>Participar en futuros estudios de la marca.</li>
											</ul>
										</div>
									</p>
									<h2 class="open-sans-bold font24"><br></h2>
								<h2 class="open-sans-bold font24">IV. NEGATIVA AL TRATAMIENTO PARA FINALIDADES SECUNDARIAS</h2>
								<p class="open-sans-light font14">Si desea que sus datos personales no sean utilizados para todas o alguna(s) de las finalidades secundarias establecidas en el presente Aviso de Privacidad Integral para Sitio Web, deberá enviarnos un correo electrónico a la dirección <strong class="m-gold"><a href="mailto:privacidadinfo@alsea.com.mx" class="m-gold">privacidadinfo@alsea.com.mx</a></strong> indicándonos lo siguiente:</p>
								<p class="open-sans-light font14">
									<br>
									No consiento que mis datos personales se utilicen para las siguientes Finalidades secundarias:
									<br>
									<div class="edit-list">
										<ul class="browser-default open-sans-light font14">
											<li>Envío de sus promociones y descuentos (publicidad y mercadotecnia).</li>
											<li>Realizar trivias, concursos y sorteos mediante redes sociales (Facebook, Twitter e Instagram).</li>
											<li>Poder identificarle y hacer entrega de premios a los ganadores de nuestros concursos y sorteos.</li>
											<li>Envío de felicitaciones y promociones el día de su cumpleaños.</li>
											<li>Participar en futuros estudios de la marca.</li>
										</ul>
									</div>
								</p>
								<p class="open-sans-light font14"><br>
									El manifestar su negativa al tratamiento de sus datos personales para finalidades secundarias, no &nbsp;será motivo para dejar de brindarle nuestros servicios.
								<br></p>
								<div>&nbsp;</div>
								<h2 class="open-sans-bold font24">V. TRANSFERENCIAS</h2>
									<p class="open-sans-light font14">Con fundamento en lo dispuesto en el artículo 37, de la Ley Federal de Protección de Datos Personales en Posesión de los Particulares, sus datos personales podrán ser transferidos lícitamente a terceros sin necesidad de su consentimiento únicamente en los siguientes casos: (i) cuando se trate de empresas nacionales o internacionales del mismo grupo, como subsidiarias, filiales, afiliadas, controladas o controladoras bajo el control común del responsable o a una sociedad matriz o a cualquier sociedad del mismo grupo del responsable que opere bajo los mismos procesos y políticas internas, (ii) cuando la transferencia sea necesaria por virtud de un contrato celebrado o por celebrar en interés del titular, por el responsable y un tercero, (iii) cuando la transferencia sea necesaria o legalmente exigida por alguna autoridad para salvaguardar un interés público, o para la procuración o administración de justicia, (iv) cuando la transferencia sea precisa para el reconocimiento, ejercicio o defensa de un derecho en un proceso judicial y (v) cuando la transferencia sea precisa para el mantenimiento o cumplimiento de una relación jurídica entre el responsable y el titular.</p>
									<br><div>&nbsp;</div>
									<p class="open-sans-light font14">Salvo en los casos antes señalados y los demás referidos en el artículo 37 de la Ley Federal de Protección de Datos Personales en Posesión de los Particulares, su información personal no será transferida a terceros.</p>
									<div>&nbsp;</div>
								<div><h2 class="open-sans-bold font24">VI. DERECHOS ARCO</h2></div>
									<p class="open-sans-light font14">Como titular de los datos personales recabados, tiene derecho a conocer con que información contamos, así como el uso y tratamiento que se le da a la misma. De igual manera tiene derecho solicitar la corrección de su información personal en caso de que esté desactualizada, sea inexacta o incompleta (Rectificación); que eliminemos su información, de nuestros registros o bases de datos cuando considere que la misma no está siendo utilizada conforme a los principios, deberes y obligaciones previstos en la normativa aplicable (Cancelación); así mismo puede oponerse al uso de sus datos personales para fines específicos (Oposición). Estos derechos se conocen como derechos ARCO.</p>
									<br>&nbsp;
									<p class="open-sans-light font14">Usted puede ejercer los derechos anteriormente mencionados a través del formulario <strong>“Solicitud para Atención a los Requerimientos ARCO”</strong>, para esto deberá enviar un correo electrónico a la dirección <strong class="m-gold"><a href="mailto:privacidadinfo@alsea.com.mx" class="m-gold">privacidadinfo@alsea.com.mx</a></strong> solicitando el formulario correspondiente y seguir el procedimiento que será enviado junto con su solicitud; o bien si lo desea puede acudir directamente con el Departamento de Privacidad de Datos que se encuentra ubicado en Avenida Revolución número 1267, Torre Corporativa, Piso 21, Colonia Los Alpes, Delegación Álvaro Obregón, Código Postal 01040, en la Ciudad de México,o ponerse en contacto al teléfono (55) 75832000 ext.13021, en donde se atenderá cualquier duda respecto al tratamiento de su información y se dará trámite a las solicitudes para el ejercicio de estos derechos.</p><div>&nbsp;</div>
								<div><h2 class="open-sans-bold font24">VII. REVOCACIÓN DE CONSENTIMIENTO</h2></div>
									<p class="open-sans-light font14">Usted puede revocar el consentimiento que nos haya otorgado para el tratamiento de sus datos personales.</p>
									<div>&nbsp;</div><p class="open-sans-light font14">Sin embargo, es importante que tenga en cuenta que no en todos los casos podremos atender su solicitud o concluir el uso de tus datos personales de forma inmediata, ya que es posible que por alguna obligación legal requiramos seguir tratando sus datos personales. Asimismo, debe considerar que para ciertos fines, la revocación de su consentimiento implicará que no podamos seguir brindándole el servicio que nos solicita.</p>
									<div>&nbsp;</div><p class="open-sans-light font14">Para solicitar la revocación de su consentimiento deberá enviar un correo a la dirección <strong class="m-gold"><a href="mailto:privacidadinfo@alsea.com.mx" class="m-gold">privacidadinfo@alsea.com.mx</a></strong> solicitando el <strong>“Formato del Revocación del Consentimiento”</strong> y seguir el procedimiento que será enviado junto con su solicitud.</p>
									<div>&nbsp;</div><p class="open-sans-light font14">Nosotros analizaremos su caso y le enviaremos respuesta a su solicitud de acuerdo a nuestro procedimiento establecido.</p>
									<div>&nbsp;</div>
								<div><h2 class="open-sans-bold font24">VIII. LIMITAR EL USO O DIVULGACIÓN DE LOS DATOS PERSONALES</h2></div>
									<p class="open-sans-light font14">Si desea limitar el uso o divulgación de los datos personales que nos ha otorgado con fines de publicidad y/o mercadotecnia, envíenos un correo a la dirección <strong class="m-gold"><a href="mailto:privacidadinfo@alsea.com.mx" class="m-gold">privacidadinfo@alsea.com.mx</a></strong>, o puede darse de baja automáticamente desde los correos que le enviaremos, haciendo clic en la parte “Remover suscripción” o “Unsuscribe”.</p>
									<div>&nbsp;</div><p class="open-sans-light font14">Nos comprometemos a incluir su información en un Listado de Exclusión para suspender toda actividad relacionada con el ofrecimiento de servicios (publicidad y/o mercadotecnia) que pudiera recaer en actos de molestia respecto del uso de su información.</p>
									<p class="open-sans-light font14">&nbsp;</p>
								<div><h2 class="open-sans-bold font24">IX. EL USO DE TECNOLOGÍAS DE RASTREO EN NUESTRO PORTAL DE INTERNET</h2></div>
									<div style="color: #B7843B; font-family: Verdana; font-size: 11px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;">
									<div>Le informamos que en nuestra página de Internet utilizamos cookies, web beacons y otras tecnologías a través de las cuales es posible monitorear su comportamiento como usuario de Internet, para brindarle un mejor servicio y experiencia al navegar en nuestra página, así como para ofrecerle nuevos productos y servicios basados en sus preferencias.</div>
									</div>
									<p class="open-sans-light font14">Los datos personales que obtenemos de éstas tecnologías de rastreo son los siguientes: Navegador utilizado, nombre del servidor, dirección IP, fecha horario y tiempo de navegación en internet y en nuestro sitio, secciones consultadas, y páginas de Internet accedidas cuando estas en nuestro sitio web.</p>
									<p class="open-sans-light font14">Estas tecnologías podrán deshabilitarse siguiendo los siguientes pasos: 1. Acceder a nuestra página de Internet, 2. Dar clic en la subsección “Cookies” de su navegador; 3. Dar clic en la leyenda de activar el mecanismo de <strong>deshabilitación de cookies.</strong></p>
									<p class="open-sans-light font14">Para mayor información sobre el uso de estas tecnologías, puedes consultar nuestra <strong>“Política de sitio web”</strong> que se encuentra en este mismo sitio de Internet.</p>
									<p class="open-sans-light font14"><br></p>
								<div><div><h2 class="open-sans-bold font24">X. DEPARTAMENTO DE PRIVACIDAD DE DATOS PERSONALES</h2></div></div>
									<div>
										<div><p class="open-sans-light font14">En caso de que usted tenga alguna duda sobre este Aviso de Privacidad Integral para Sitio Web, o en su caso del tratamiento que les estamos dando a sus Datos Personales, póngase en contacto con nuestro <strong>Departamento de Privacidad de datos personales,</strong> donde con gusto te atenderemos:</p></div>
										<div><p class="open-sans-light font14"><strong>Domicilio:</strong> Avenida Revolución número 1267, Torre Corporativa, Piso 21, Colonia Los Alpes, Delegación Álvaro Obregón, Código Postal 01040, en la Ciudad de México.</p></div>
										<div><p class="open-sans-light font14"><strong>Correo electrónico:</strong><strong class="m-gold"><a href="mailto:privacidadinfo@alsea.com.mx" class="m-gold">privacidadinfo@alsea.com.mx</a></strong></p></div>
										<div><p class="open-sans-light font14"><strong>Teléfono:</strong> (55) 75832000 ext.13021</p></div>
									</div>
								<div>&nbsp;</div>
								<div style="color: #FFF; font-family: Verdana; font-size: 11px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;">
									<p style="orphans: 2; text-align: start; text-indent: 0px; widows: 2; text-decoration-style: initial; text-decoration-color: initial;">
										<p style="color: #FFF; font-family: Verdana; font-size: 11px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px;">
											<h2 class="open-sans-bold font24">XI. ACTUALIZACIÓN DEL AVISO DE PRIVACIDAD</h2>
										</p>
									</p>
								</div>
								<p class="open-sans-light font14" style="color: #FFF;">El presente Aviso de Privacidad Integral podrá ser modificado o actualizado en cualquier momento; en caso de que se realice alguna modificación que requiera poner a su disposición un nuevo aviso, se le hará saber mediante correo electrónico a la cuenta de correo que se proporcionó inicialmente, en caso de ser sólo una actualización podrá consultarlo en nuestro portal de internet <a target="_blank" href="http://www.starbucks.com.mx/" class="m-gold">http://www.starbucks.com.mx/</a></p>
								<br><br><br>
								<p class="open-sans-light font14" style="color: #B7843B;"><strong>Fecha de actualización: Junio de 2017.</strong></p>
								<div class="row">
									<div class="space40"></div>
								</div>
							</div>
						</article>
					</div><div class="gradientback"></div>
				</div>
			</div>
		</div>