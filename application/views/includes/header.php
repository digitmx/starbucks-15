<?php
    $controller_slug = $this->uri->segment(1);
    if (!$controller_slug) { $controller_slug = 'home'; }
    
    //Facebook Open Graph
    $ogTitle = "STARBUCKS 15 AÑOS";
	$ogUrl = "https://starbucks15.mx";
	$ogSiteName = "STARBUCKS 15 AÑOS";
	$ogImage = "https://starbucks15.mx/assets/img/fb_share.jpg";
	$ogType = "website";
	$ogDescription = "Entra al sitio, registra tu historia y participa para ganar los premios únicos que tenemos para ti.";
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js ie lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js ie lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js ie lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js ie lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="es"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta name="robots" content="noindex,nofollow">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
		<meta name="format-detection" content="telephone=no">
		<meta name="apple-mobile-web-app-title" content="Starbucks 15 años">
		<title>Starbucks 15 años</title>
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>assets/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url(); ?>assets/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/favicon/favicon-16x16.png">
		<link rel="manifest" href="<?php echo base_url(); ?>assets/favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo base_url(); ?>assets/favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<link rel="stylesheet" id="styles-css" href="<?php echo base_url(); ?>assets/css/app.css" type="text/css" media="all">
        <!-- Facebook Open Graph -->
        <meta property="fb:app_id" content="227714991077726" />
        <meta property="og:title" content="<?php echo $ogTitle; ?>"/>
		<meta property="og:url" content="<?php echo $ogUrl; ?>"/>
		<meta property="og:site_name" content="<?php echo $ogSiteName; ?>"/>
		<meta property="og:image" content="<?php echo $ogImage; ?>"/>
		<meta property="og:type" content="<?php echo $ogType; ?>"/>
		<meta property="og:description" content="<?php echo $ogDescription; ?>"/>
        <!-- Twitter Open Graph -->
        <meta name="twitter:card" content="summary_large_image" />
		<meta name="twitter:site" content="@StarbucksMex" />
		<meta name="twitter:creator" content="@StarbucksMex" />
		<meta property="twitter:title" content="STARBUCKS 15 AÑOS" />
		<meta property="twitter:description" content="Entra al sitio, registra tu historia y participa para ganar los premios únicos que tenemos para ti." />
		<meta property="twitter:image" content="https://starbucks15.mx/assets/img/tw_share.jpg" />
        <script>(function(h){h.className = h.className.replace('no-js', 'js')})(document.documentElement)</script>
		<!-- HTML5 shim and Respond.js IE8 support for HTML5 elements and media queries. -->
		<!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>assets/js/html5shiv-printshiv.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/respond.min.js"></script>
        <![endif]-->
        <?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false): ?>
        <script type="text/javascript">
			setTimeout(function(){var a=document.createElement("script");
			var b=document.getElementsByTagName("script")[0];
			a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0068/4177.js";
			a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
		</script>
        
        <script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-91308992-3', 'auto');
		  ga('send', 'pageview');
		
		</script>
		<?php endif; ?>
	</head>
	<body class="<?php echo $controller_slug; ?>">
		<?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false): ?>
		<!--[if lt IE 8>
			<p class="browsehappy">
				You are using an <strong>outdated</strong> browser.
				Please <a href="http://browsehappy.com/">upgrade your browser</a>
				to improve your experience.
			</p>
	    <![endif]-->
	    <!-- Google Code for CONCURSO 15 A&Ntilde;OS Conversion Page -->
		<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 877700512;
		var google_conversion_language = "en";
		var google_conversion_format = "3";
		var google_conversion_color = "ffffff";
		var google_conversion_label = "W1lYCJuOrHQQoMvCogM";
		var google_remarketing_only = false;
		/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/877700512/?label=W1lYCJuOrHQQoMvCogM&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
		<!-- Facebook Pixel Code -->
		<script>
			!function(f,b,e,v,n,t,s)
			{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};
			if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
			n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t,s)}(window,document,'script',
			'https://connect.facebook.net/en_US/fbevents.js');
			fbq('init', '1732771300350683');
			fbq('track', 'PageView');
		</script>
		<noscript>
			<img height="1" width="1" src="https://www.facebook.com/tr?id=1732771300350683&ev=PageView&noscript=1"/>
		</noscript>
		<!-- End Facebook Pixel Code -->
		<?php endif; ?>
		<div class="navbar-fixed">
			<nav class="hide-on-large-only transparent">
				<div class="nav-wrapper">
					<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
					<ul class="side-nav open-sans half-transparent" id="mobile-demo">
		  				<li><a class="white-text" href="<?php echo base_url(); ?>">Historias ganadoras</a></li>
		  				<li><a class="white-text" href="<?php echo base_url(); ?>premios">Premios</a></li>
		  				<li><a class="white-text" href="<?php echo base_url(); ?>terminosycondiciones">Términos y Condiciones</a></li>
		  				<li><a class="white-text" href="<?php echo base_url(); ?>avisodeprivacidad">Aviso de Privacidad</a></li>
	      			</ul>
	    		</div>
	  		</nav>
		</div>
  		