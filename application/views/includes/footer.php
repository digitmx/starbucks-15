		
		<!--Menu Footer-->
		<div class="container-fluid black">
			<div class="container footer">
				<div class="row">
					<div class="col s12 font16 centered">
						<div class="col s12 m3 l3">
							<a href="<?php echo base_url(); ?>">Historias ganadoras</a>
						</div>
						<div class="col s12 m3 l3 grey lighten-1">
							<a class="black-text" href="<?php echo base_url(); ?>premios">Premios</a>
						</div>
						<div class="col s12 m3 l3">
							<a href="<?php echo base_url(); ?>terminosycondiciones">Términos y Condiciones</a>
						</div>
						<div class="col s12 m3 l3">
							<a href="<?php echo base_url(); ?>avisodeprivacidad">Aviso de Privacidad</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<input type="hidden" id="base_url" name="base_url" value="<?php echo base_url(); ?>" />

		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app.js"></script>

	</body>
</html>