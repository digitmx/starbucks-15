<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// How to send the response header with PHP
if (isset($_SERVER['HTTP_ORIGIN'])) {
		header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

		exit(0);
}

ini_set('memory_limit', '20480M');
ini_set('max_execution_time', 1000000);

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Common\Aws;
use Aws\Common\Enum\Size;
use Aws\S3\Enum\CannedAcl;
use Aws\Common\Enum\Region;
use Aws\Common\Exception\MultipartUploadException;
use Aws\S3\Model\MultipartUpload\UploadBuilder;
use Aws\Common\Model\MultipartUpload\AbstractUploadBuilder;

class Engine extends CI_Model {
	
	public $bucket = 'holcim-app'; //nombre del bucket

    private $_key = 'AKIAIGA3MI32SFTQGHBQ'; // tu key

    private $_secret = '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'; // tu secret

    public $s3 = null;

	public function __construct()
	{
		parent::__construct();
		
		$this->s3 = S3Client::factory(array(
			'version' => '2006-03-01',
			'region' => 'us-west-2',
            'credentials' => array(
                'key'    => $this->_key,
                'secret' => $this->_secret,
            )
        ));
	}

	//Funcion para identar JSON
	public function indent($json)
	{
	    $result      = '';
	    $pos         = 0;
	    $strLen      = strlen($json);
	    $indentStr   = '  ';
	    $newLine     = "\n";
	    $prevChar    = '';
	    $outOfQuotes = true;

	    for ($i=0; $i<=$strLen; $i++) {

	        // Grab the next character in the string.
	        $char = substr($json, $i, 1);

	        // Are we inside a quoted string?
	        if ($char == '"' && $prevChar != '\\') {
	            $outOfQuotes = !$outOfQuotes;

	        // If this character is the end of an element,
	        // output a new line and indent the next line.
	        } else if(($char == '}' || $char == ']') && $outOfQuotes) {
	            $result .= $newLine;
	            $pos --;
	            for ($j=0; $j<$pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        // Add the character to the result string.
	        $result .= $char;

	        // If the last character was the beginning of an element,
	        // output a new line and indent the next line.
	        if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
	            $result .= $newLine;
	            if ($char == '{' || $char == '[') {
	                $pos ++;
	            }

	            for ($j = 0; $j < $pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        $prevChar = $char;
	    }

	    return $result;
	}

	public function printJSON($array)
	{
		$json = json_encode($array);
		header('Content-Type: application/json',true);
		echo $this->indent($json);
	}

	//Manejador de Mensajes Recibidos en el JSON
	public function executeJSON($msg,$fields,$app,$apikey)
	{
		//Leemos la Configuracion
		$output = FALSE;

		// loginAdmin
		if ($msg == 'loginAdmin')
		{
			//Leemos los datos
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';

			//Verificamos los datos
			if ($email && $password)
			{
				//Consultamos los datos
				$query = $this->db->query("SELECT * FROM admin WHERE email = '" . $email . "' AND password = '" . sha1($password) . "' AND status = 1 LIMIT 1");

				//Verificamos si existe el usuario
				if ($query->num_rows() > 0)
				{
					//Leemos el Objeto
					$row = $query->row();

					//Consultamos
					$data = array(
						'idadmin' => $row->idadmin,
					    'email' => $row->email,
					    'name' => $row->name,
					    'profile' => $row->profile
					);

					//Save Admin in $_SESSION
					$this->session->set_userdata('admin', $data);
					$this->session->set_userdata('haap_logged', true);

					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $data
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Credenciales incorrectas. Intenta de nuevo.'
					);

					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);

				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		if (!$output)
		{
			//Mostrar Error
			$array = array(
				'status' => (int)0,
				'msg' => (string)'Llamada inválida.'
			);

			//Imprimimos el Arreglo
			$this->printJSON($array);
			$output = TRUE;
		}
	}
	
}