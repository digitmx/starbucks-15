<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class facebook extends CI_Model 
{
	public $fb ;
	public $access_token ;

	public function __construct()
	{
    	parent::__construct();
    }

	public function facebookInit()
	{
      	$this->fb = new Facebook\Facebook([
            'app_id' => '227714991077726',
            'app_secret' => 'e7365501cb7c1adfaf13a20784ae3f65',
            'default_graph_version' => 'v2.9',
            'persistent_data_handler' => 'session'
        ]); 
		return $this->fb;                        
	}
	
	//function to get login url for your app . Just create a controller which     handles redirects after facebook login 
	public function loginUrl()
	{
		$params = ($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : '';
    	$helper = $this->facebookInit()->getRedirectLoginHelper();
		$permissions = ['email','public_profile']; // Optional permissions

		$loginUrl = $helper->getLoginUrl(base_url()."response/".$params, $permissions);
		return $loginUrl ;  
	}
	
	public function loginUrlEvent()
	{
		$params = ($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : '';
    	$helper = $this->facebookInit()->getRedirectLoginHelper();
		$permissions = ['email','public_profile']; // Optional permissions

		$loginUrl = $helper->getLoginUrl(base_url()."response/event/".$params, $permissions);
		return $loginUrl ;  
	}
	
	public function logoutUrl()
	{
		$next = base_url().'activacion';
    	$helper = $this->facebookInit()->getRedirectLoginHelper();
    	$accessToken = $helper->getAccessToken();
		
		$logoutUrl = $helper->getLogoutUrl($_SESSION['fb_token_activacion'],$next,'&');
		return $logoutUrl ;  
	}
	
	public function response()
	{
		$helper = $this->facebookInit()->getRedirectLoginHelper();
		
		try {
			$accessToken = $helper->getAccessToken();
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		  	// When Graph returns an error
		  	echo 'Graph returned an error: ' . $e->getMessage();
		  	exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  	// When validation fails or other local issues
		  	echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  	exit;
		}
		
		if (! isset($accessToken)) {
			if ($helper->getError()) {
		    	header('HTTP/1.0 401 Unauthorized');
				echo "Error: " . $helper->getError() . "\n";
				echo "Error Code: " . $helper->getErrorCode() . "\n";
				echo "Error Reason: " . $helper->getErrorReason() . "\n";
				echo "Error Description: " . $helper->getErrorDescription() . "\n";
		  	} else {
		    	header('HTTP/1.0 400 Bad Request');
				return 'Bad request';
		  	}
		  	exit;
		}
		
		// Logged in
		//echo '<h3>Access Token</h3>';
		//var_dump($accessToken->getValue());
		
		try {
		  // Returns a `Facebook\FacebookResponse` object
		  $response = $this->facebookInit()->get('/me?fields=id,email,first_name,last_name', $accessToken->getValue());
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		  //return 'Graph returned an error: ' . $e->getMessage();
		  //exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  //return 'Facebook SDK returned an error: ' . $e->getMessage();
		  //exit;
		}
		
		$user = $response->getGraphUser();
		
		//Generamos el Elemento
		$array = array(
			'id' => isset($user['id']) ? $user['id'] : '0',
			'email' => isset($user['email']) ? $user['email'] : '',
			'first_name' => isset($user['first_name']) ? $user['first_name'] : '',
			'last_name' => isset($user['last_name']) ? $user['last_name'] : '',
			'token' => $accessToken->getValue()
		);
		
		return $array;
	}

}