
	function remove_file(e)
	{
		var div = e.parentElement;
		var input = div.children[0];
		var document_images_size = document.getElementById("image_size");
		var images_size = parseFloat(document_images_size.value);
		var image_size = 0;
		var tmp_size = 0;
		
		if (input.files.length)
		{
			for (i = 0; i < input.files.length; i++) { 
				var result = input.files[i].type.split('/');
				image_size = image_size + input.files[i].size;
			}
			
			tmp_size = parseFloat(images_size) - parseFloat((image_size / (1024*1024)).toFixed(2));
			document_images_size.value = tmp_size.toString();
		}
		
		$(e).parent().remove();
	}
	
	function check_video(e)
	{
		//Var Lenght
		var count_video = e.files.length;
		var video_size = 0;
		
		//Check Lenght
		if (count_video)
		{
			var video_flag = 1;
			var result = e.files[0].type.split('/');
			video_size = e.files[0].size;
			if (result[0] != 'video') { video_flag = 0; }
			
			video_size = parseFloat((video_size / (1024*1024)).toFixed(2));
			
			if (video_flag && video_size <= 100)
			{
				//Success
				Materialize.toast('Tu video fue cargado exitosamente.', 4000);
			}
			else
			{
				//Delete File
				$(e).val("");
				
				if (!video_flag)
				{
					//Error
					Materialize.toast('Tu video no es válido. Intenta con otro video.', 4000);
				}
				else
				{
					//Error
					Materialize.toast('El video seleccionado pesa más de 100mb. Vuelve a intentarlo.', 4000);
				}
			}
		}
	}
	
	function check(e)
	{
		//Var Lenght
		var count_photos = e.files.length;
		var document_images_size = document.getElementById("image_size");
		var images_size = parseFloat(document_images_size.value);
		var tmp_size = 0;
		
		//Check Lenght
		if (count_photos)
		{
			var image_flag = 1;
			var image_size = 0;
			
			for (i = 0; i < e.files.length; i++) { 
				var result = e.files[i].type.split('/');
				image_size = image_size + e.files[i].size;
				if (result[0] != 'image') { image_flag = 0; }
			}
			
			//Check Size
			tmp_size = parseFloat(images_size) + parseFloat((image_size / (1024*1024)).toFixed(2));
			
			if (image_flag && tmp_size <= 10)
			{
				document_images_size.value = tmp_size.toString();
				
				if (count_photos == 1)
				{
					//Success
					Materialize.toast('Tu foto fue cargada exitosamente.', 4000);
				}
				else
				{
					//Success
					Materialize.toast('Tu foto no es válida. Intenta con otro imagen.', 4000);
				}
			}
			else
			{
				//Delete File
				$(e).val("");
				
				if (!image_flag)
				{
					//Error
					Materialize.toast('El archivo seleccionado no es imagen. Intenta con otra imagen.', 4000);
				}
				else
				{
					//Error
					Materialize.toast('Las imágenes seleccionadas pesan más de 10mb. Vuelve a intentarlo.', 4000);
				}
			}
		}
	}
	
	$( document ).ready(function() {
		
		//Read Data
		var base_url = $('#base_url').val();
		
		$.ajaxSetup({ cache: true });
		$.getScript('//connect.facebook.net/en_US/sdk.js', function(){
		    FB.init({
		    	appId: '227714991077726',
				version: 'v2.7' // or v2.1, v2.2, v2.3, ...
		    });     
		    $('#loginbutton,#feedbutton').removeAttr('disabled');
		});
		
		//Fix Fields Facebook
		$('#inputEmail').change(function(e) { $('#inputEmail').attr("placeholder", "Correo electrónico"); });
		$('#inputName').change(function(e) { $('#inputName').attr("placeholder", "Nombre"); });
		$('#inputLastname').change(function(e) { $('#inputLastname').attr("placeholder", "Apellido"); });
		
		function fbAuthUser() {
			
			//GA
			ga('send', 'event', 'Registros', 'Registro Social Media ', 'Registro facebook');
			console.log('ga');
			
		    FB.login(function(response) {
			 
			    if(response && response.status == 'connected') 
				{
					//Leemos los Datos
				    FB.api('/me?fields=id,email,first_name,last_name,gender', function(response) {
					    if (typeof response.email !== 'undefined') { $('#inputEmail').val(response.email); $('#inputEmail').removeAttr('placeholder'); }
					    if (typeof response.first_name !== 'undefined') { $('#inputName').val(response.first_name); $('#inputName').removeAttr('placeholder'); }
					    if (typeof response.last_name !== 'undefined') { $('#inputLastname').val(response.last_name); $('#inputLastname').removeAttr('placeholder'); }
					    if (typeof response.gender !== 'undefined')
					    { 
						    if (response.gender == 'male') { $('#inputGenre').val('Masculino'); } 
						    if (response.gender == 'female') { $('#inputGenre').val('Femenino'); }
						}
						
						//Ocultamos el Botón
						$('.boton-enviar-f').fadeOut('slow');
				    });
				}
				else
				{
					//Success
					Materialize.toast('Inicio de Sesión Cancelado.', 4000);
				}
				
			}, {scope: 'email'});
		}
		
		//Modal
		$('.modal').modal({
			dismissible: false,
			ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
				console.log(modal, trigger);
				player.playVideo();
			},
			complete: function() { stopVideo(); } // Callback for Modal close
		});
		
		//Sidenav
		$(".button-collapse").sideNav();
		
		//hover info
		$('.home .info').hover(function(e){
			$( ".modal-trigger" ).trigger( "click" );
		});
		
		//Premios 
		$('.imagen-premios-e1 .slider').slider({
			indicators: false,
			interval: 3000
		});
		
		$('.imagen-premios-e2 .slider').slider({
			indicators: false,
			interval: 3000
		});
		
		//Click btnFormHistory
		$('#btnFormHistory').on('click', function(e) {
			e.preventDefault();
			
			//Disable Button
			$('#btnFormHistory').prop( 'disabled', true );
			$('#btnFormHistory').html('PROCESANDO...');
			
			//Read Values
			var name = $.trim($('#inputName').val());
			var lastname = $.trim($('#inputLastname').val());
			var genre = $.trim($('#inputGenre').val());
			var email = $.trim($('#inputEmail').val());
			var state = $.trim($('#inputState').val());
			var phone = $.trim($('#inputPhone').val());
			var product = $.trim($('#inputProduct').val());
			var history = $.trim($('#inputHistory').val());
			var role = $.trim($('#inputRole').val());
			var sb1 = $.trim($('#inputSB1').val());
			var sb2 = $.trim($('#inputSB2').val());
			var sb3 = $.trim($('#inputSB3').val());
			var sb4 = $.trim($('#inputSB4').val());
			var mysbrewards = sb1.toString() + sb2.toString() + sb3.toString() + sb4.toString();
			var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			var regex_phone = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
			var regex_card = /^\d{4}$/;
			var sb_flag = 0;
			
			//Validate Fields
			if (name && lastname && genre && email && state && phone && product && history && role)
			{
				//Validate Email
				if (regex.test(email))
				{
					//Validate Phone
					if (regex_phone.test(phone))
					{
						//Validate Checkbox Terms
						if ($("#inputTerms").is(":checked")) 
						{
							//Validate Checkbox Privacy
							if ($("#inputPrivacy").is(":checked")) 
							{
								//Check Card
								if (sb1 || sb2 || sb3 || sb4)
								{
									if (sb1.length && regex_card.test(sb1))
									{
										if (sb2.length && regex_card.test(sb2))
										{
											if (sb3.length && regex_card.test(sb3))
											{
												if (sb4.length && regex_card.test(sb4))
												{
													//Update Flag
													sb_flag = 1;
												}
											}	
										}
									}
								}
								else
								{
									//Update Flag
									sb_flag = 1;
								}
								
								//Check Flag
								if (sb_flag)
								{
									//GA
									ga('send', 'event', 'Registros', 'Registro formulario', 'Se envía el formulario');
									console.log('ga');
			
									//Send Data
									$('#formHistory').submit();
								}
								else
								{
									//Error
									Materialize.toast('La información de My Starbucks Rewards no es correcta. Verifica el número de tu tarjeta.', 4000);
								}
							}
							else
							{
								//Error
								Materialize.toast('Para participar tienes que aceptar el aviso de privacidad.', 4000);
							}
						}
						else
						{
							//Error
							Materialize.toast('Para participar tienes que leer y aceptar los términos y condiciones.', 4000);
						}
					}
					else
					{
						//Error
						Materialize.toast('Escribe un teléfono válido de 10 dígitos.', 4000);
					}
				}
				else
				{
					//Error
					Materialize.toast('Escribe un correo electrónico válido.', 4000);
				}
			}
			else
			{
				//Error
				Materialize.toast('Todos los campos son obligatorios.', 4000);
			}
			
			//Disable Button
			$('#btnFormHistory').prop( 'disabled', false );
			$('#btnFormHistory').html('ENVIAR');
			
			return false;
		});
		
		//Click btnFormMedia
		$('#btnFormMedia').on('click', function(e) {
			e.preventDefault();
			
			//Read MySBRewards
			var mysbrewards = $('#inputSB').val();
			var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			var regex_card = /^\d{16}$/;
			var sb_flag = 0;
			
			//Check Value
			if (!mysbrewards)
			{
				//Send Data
				$('#formMedia').submit();
			}
			else
			{
				//Check Email
				if (regex.test(mysbrewards))
				{
					//Update Flag
					sb_flag = 1;
				}
				
				//Check Card
				if (regex_card.test(mysbrewards))
				{
					//Update Flag
					sb_flag = 1;
				}
				
				//Check Flag
				if (sb_flag)
				{
					//Send Data
					$('#formMedia').submit();
				}
				else
				{
					//Error
					Materialize.toast('La información de My Starbucks Rewards es errónea.', 4000);
				}
			}
			
			return false;
		});
		
		//binds to onchange event of your input field
		$('.fotos-video-btn #images').bind('change', function() {
			
			//Var Lenght
			var count_photos = this.files.length;
			
			//Check Lenght
			if (count_photos)
			{
				var image_flag = 1;
				var images_size = 0;
				
				$.each(this.files, function( k, v ) {
					var result = v.type.split('/');
					images_size = images_size + v.size;
					if (result[0] != 'image') { image_flag = 0; }
				});
				
				images_size = (images_size / (1024*1024)).toFixed(2);
				
				if (image_flag && images_size <= 10)
				{
					if (count_photos == 1)
					{
						//Success
						Materialize.toast('Tu foto fue cargada exitosamente.', 4000);
					}
					else
					{
						//Success
						Materialize.toast('Tu foto no es válida. Intenta con otro imagen.', 4000);
					}
				}
				else
				{
					//Delete File
					$(this).val("");
					
					if (!image_flag)
					{
						//Error
						Materialize.toast('Uno o más archivos no son imágenes. Intenta con otro imagen.', 4000);
					}
					else
					{
						//Error
						Materialize.toast('Las imágenes seleccionadas pesan más de 10mb. Vuelve a intentarlo.', 4000);
					}
				}
			}
			else
			{
				//Delete File
				$(this).val("");
				
				//Error
				Materialize.toast('Selecciona mínimo una foto.', 4000);
			}
		});
		
		$('.fotos-btn').on('click', function(e) {
			e.preventDefault();
			
			//GA
			ga('send', 'event', 'Update Material', 'Subir fotos', 'Subir fotos de historia');
			console.log('ga');
			
			$(".home .file-images-list").append("<div><input type='file' name='images[]' onchange=check(this);><input type='button' value='X' onclick=remove_file(this);></div>");
			
			return false;
		});
		
		$('.fotos-video-btn').on('click', function(e) {
			e.preventDefault();
			
			//GA
			ga('send', 'event', 'Update Material', 'Subir video', 'Subir videos de historia');
			console.log('ga');
			
			var video_counter = parseInt($('.fotos-video-btn #video_counter').val());
			
			if (!video_counter)
			{
				video_counter++;
				$(".home .file-video-list").append("<div><input type='file' name='video' onchange=check_video(this);></div>");
				$('.fotos-video-btn #video_counter').val(video_counter.toString());
			}
			else
			{
				//Error
				Materialize.toast('Solo se puede agregar un video a tu historia.', 4000);
			}
			
			return false;
		});
		
		//binds to onchange event of your input field
		$('.fotos-video-btn #video').bind('change', function() {
		
			//Read Size
			var size = this.files[0].size;
			var size_mb = (size / (1024*1024)).toFixed(2);
			
			if (size_mb < 100)
			{
				//Success
				Materialize.toast('Tu video fue cargado exitosamente.', 4000);
			}
			else
			{
				//Delete File
				$(this).val("");
				
				//Error
				Materialize.toast('Tu video no es válido. Intenta con otro video.', 4000);
			}
		});
		
		//boton-enviar-f click
		$('.boton-enviar-f a').on('click', function(e) {
			e.preventDefault();
			
			fbAuthUser();
			
			return false;
		});
		
		//btnShareTwitter Click
		$('.btnShareTwitter').on('click', function(e) {
			e.preventDefault();
			
			//GA
			//ga('send', 'event', 'Share content', 'Compartir registro', 'Compartir registo twitter');
			ga('send', 'event', 'Social media', 'Shares historia twitter', 'RT historia finalista');
			console.log('ga');
			
			var width  = 575,
	            height = 400,
	            left   = ($(window).width()  - width)  / 2,
	            top    = ($(window).height() - height) / 2,
	            url    = 'http://twitter.com/share?url=' + base_url + '&text=Me encantó esta historia finalista del concurso %2315AñosJuntos de @starbucksmex. Descúbrela y vota por ella aquí: ',
	            opts   = 'status=1' +
	                     ',width='  + width  +
	                     ',height=' + height +
	                     ',top='    + top    +
	                     ',left='   + left;

	        window.open(url, 'twitter', opts);

	        return false;
	    });

		//btnShareFacebook Click
	    $('.btnShareFacebook').on('click', function(e) {
		    e.preventDefault();
			
			//GA
			//ga('send', 'event', 'Share content', 'Compartir registro', 'Compartir registo facebook');
			ga('send', 'event', 'Social media', 'Shares historia facebook', 'Shares historia finalista');
			console.log('ga');
									
			FB.ui({
				method: 'share',
				href: base_url,
				hashtag: '',
				quote: 'Me encantó esta historia finalista del concurso #15AñosJuntos de @Starbucks México. Descúbrela y vota por ella aquí',
				mobile_iframe: true,
			}, function(response){});

	        return false;
	    });
	    
	    $('.fixed-action-btn a').on('click', function(e) {
			//GA
			ga('send', 'event', 'Registros', 'Clic a registro', 'Tab lateral');
			console.log('ga');
		});
		
		$('.votacion-video form').submit(function(e) {
		    var name = $(this).attr('rel');
		    ga('send', 'event', 'Votaciones', 'Votar por historia', name.toString())
		    console.log(name);
		});
		
	});